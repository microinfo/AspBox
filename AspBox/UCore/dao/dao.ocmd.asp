<%
'######################################################################
'## dao.ocmd.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc oCmd-Dao Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/09/08 16:27
'## Description :   AspBox Mvc oCmd-Dao Block(DAO之CMD参数化查询)
'######################################################################

Class Cls_Dao_oCmd

	Private o_cmd
	Private r

	Private Sub Class_Initialize
		On Error Resume Next
		AB.Use "db"
		set o_cmd = Server.CreateObject("ADODB.Command") '实例化CMD
		o_cmd.ActiveConnection = AB.db.Conn '设置活动连接
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate
		set o_cmd = Nothing
		Err.clear
	End Sub

	'-------------------------------------------------------------------------------------------
	'# Dao.oCmd.Ra(arr)
	'# @return: integer
	'# @dowhat: 获取(最近一次执行)影响记录数
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'@ AB.Use "Mvc" : Dao.Use "oCmd"
	'@ Call Dao.oCmd.Exec("delete * from tb where id=1", "") : AB.C.PrintCn "影响的行数:" & Dao.oCmd.Ra
	'@ Call Dao.oCmd.Exec("delete * from tb where id=?", "1") : AB.C.PrintCn "影响的行数:" & Dao.oCmd.Ra
	'@ Call Dao.oCmd.Exec("delete * from tb where id =? and name=?",array("1","admin")) : AB.C.PrintCn "影响的行数:" & Dao.oCmd.Ra
	'------------------------------------------------------------------------------------------

	Public Property Get Ra
		Ra = r
	End Property

	'-------------------------------------------------------------------------------------------
	'# Dao.oCmd.Exec(arr)
	'# @return: Object(RecordSet对象)
	'# @dowhat: 执行一个sql语句参数并返回RecordSet对象
	'# 			返回一个sql执行后的结果,具体由运行的sql语句决定
	'--DESC------------------------------------------------------------------------------------
	'# @param sql: [string] (字符串) 执行的SQL语句
	'# @param args: [array | string] (数组或字符串) 做参数数组或单个参数字符串
	'--DEMO------------------------------------------------------------------------------------
	'@ AB.Use "Mvc" : Dao.Use "oCmd"
	'@ Set Rs = Dao.oCmd.Exec("select * from [user] where id =1", "")
	'@ Set Rs = Dao.oCmd.Exec("select * from [user] where id = ?","1")
	'@ Set Rs = Dao.oCmd.Exec("select * from [user] where id = ? and name = ?",array("1","admin"))
	'------------------------------------------------------------------------------------------

	Public Function Exec(ByVal sql, ByVal args)
		set Exec = factory("Exec",sql,args)
	End Function

	'-------------------------------------------------------------------------------------------
	'# Dao.oCmd.SelAll(arr)
	'# @return: Object(RecordSet对象)
	'# @dowhat: 调用select查询table表所有字段,并返回一个执行后的recordset对象
	'# 			返回一个sql执行后的结果,具体由运行的sql语句决定
	'--DESC------------------------------------------------------------------------------------
	'# @param table: [string] (字符串) 需要查询的表名
	'--DEMO------------------------------------------------------------------------------------
	'@ AB.Use "Mvc" : Dao.Use "oCmd"
	'@ Set Rs = Dao.oCmd.SelAll("user")
	'@ AB.Trace Rs
	'------------------------------------------------------------------------------------------

	Public Function SelAll(table)
		dim sql : sql="select * from "&table&""
		set SelAll = factory("SelAll",sql,"")
	End Function

	Private Function factory(obj,sql,args)
		'obj用于发生错误时用以标志发生错误的对象
		On Error Resume Next
		dim o_exe,para,i
		o_cmd.CommandText = sql
		'o_cmd.Prepared = true
		if IsArray(args) then
			i=0
			for each a in args
				set para = o_cmd.CreateParameter(a,203,1,,a)
				o_cmd.parameters(i)=para
				i=i+1
			next
		elseif args <>"" then
			set para = o_cmd.CreateParameter(args,203,1,,args)
			o_cmd.Parameters(0)=para
		end if
		set o_exe = o_cmd.Execute(r)
		if not IsObject(o_exe) then
			checkErr(obj&"过程无法实例化，Execute运行出错！")
		end if
		If Err Then AB.ShowErr Err.Number, Err.Description
		set factory = o_exe
		On Error Goto 0
	End Function

	Private Sub checkErr(ErrMsg)
		If Err.Number <> 0 Then
			AB.ShowErr Err.Number, "自定义错误信息："&ErrMsg&"<br>系统错误信息："&Err.Description&"<br>最初生成错误的对象或应用程序的名称："&Err.Source
			Err.Clear
		End If
	End Sub

End Class
%>