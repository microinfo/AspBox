<%
'######################################################################
'## ab.error.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Error Class
'## Version     :   v1.0.0.1
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/09/27 22:42
'## Description :   Deal with the AspBox Exception
'######################################################################

Class Cls_AB_Error

	Private b_debug, b_redirect
	Private i_errNum, i_delay
	Private s_errStr, s_title, s_url, s_css, s_style, s_msg
	Private i_errCode, s_errMsg
	Private o_abe, o_err

	Private Sub Class_Initialize
		i_errNum    = ""
		i_delay     = 3000
		s_title     = "Error Occurred"
		b_debug     = AB.Debug
		b_redirect  = True
		s_url       = "javascript:history.go(-1)"
		s_css       = ""
		s_style 	= ""
		Set o_abe   = Server.CreateObject(AB.dictName)
		Set o_err   = Server.CreateObject(AB.dictName)
		o_err("Number") = 0
		o_err("Description") = ""
		o_err("Source") = ""
	End Sub

	Private Sub Class_Terminate
		Set o_abe = Nothing
		Set o_err = Nothing
	End Sub

	'-------------------------------------------------------------------------
	' @ 设置是否启用开发者调试环境，读写
	' @ AB.Error.Debug = True/False [Boolean]
	'-------------------------------------------------------------------------
	' # 此属性用于设置是否启用开发者调试环境，配置前继承自AB.Debug属性的值。
	' # 如果关闭调试环境，则程序在发生错误时不会给出详细的如错误代码等信息。
	'-------------------------------------------------------------------------

	Public Property Get [Debug]
		[Debug] = b_debug
	End Property

	Public Property Let [Debug](ByVal b)
		b_debug = b
	End Property

	Public Function Data()
		Set Data = o_abe
	End Function

	Public Default Property Get E(ByVal n)
		If o_abe.Exists(n) Then
			E = o_abe(n)
		Else
			E = "未知错误"
		End If
	End Property

	Public Property Let E(ByVal n, ByVal s)
		If AB.C.Has(n) And AB.C.Has(s) Then
			If n > "" Then
				o_abe(n) = s
			End If
		End If
	End Property

	Public Property Get LastError
		LastError = i_errNum
	End Property

	Public Property Get Title
		Title = s_title
	End Property

	Public Property Let Title(ByVal s)
		s_title = s
	End Property

	Public Property Get Msg
		Msg = s_msg
	End Property

	Public Property Let Msg(ByVal s)
		s_msg = s
	End Property

	Public Property Get [Redirect]
		[Redirect] = b_redirect
	End Property

	Public Property Let [Redirect](ByVal b)
		b_redirect = b
	End Property

	Public Property Get Url
		Url = s_url
	End Property

	Public Property Let Url(ByVal s)
		s_url = s
	End Property

	Public Property Get Delay
		Delay = i_delay / 1000
	End Property

	Public Property Let Delay(ByVal i)
		i_delay = i * 1000
	End Property

	Public Property Get ClassName
		ClassName = s_css
	End Property

	Public Property Let ClassName(ByVal s)
		s_css = s
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Error.Style 属性
	'# @syntax: style = AB.Error.Style
	'# @return: String (字符串) 获取 Css Style属性值
	'# @dowhat: 设置和获取(AspBox Error调试输出的) Css Style属性值. 此属性可读可写。
	'--DESC------------------------------------------------------------------------------------
	'# @param style : String (字符串) 用于设置Css Style属性值
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Error.Style = "fieldset#abxError{margin:0 0 0 2%;position:relative;}fieldset#abxError{padding:0 15px 10px 15px;font-size:.8em;}fieldset#abxError{background:#FFF;width:96%;margin-top:8px;padding:10px;position:relative;}fieldset#abxError{padding:0 15px 10px 15px;margin:0 0 0 2%;position:relative;}fieldset#abxError p{font-size:1.2em;margin:0 0 10px 0;}fieldset#abxError legend{font-size:1em;color:#333333;background:#E7ECF0;font-weight:bold;padding:4px 15px 4px 10px;margin:4px 0 8px -12px;_margin-top:0px;border-width:1px;border-style:solid;border-color:#EDEDED #969696 #969696 #EDEDED;}"
	'# AB.Error(10001) = "系统出错！"
	'# AB.Error.Msg = "这是附加自定义出错内容提示。"
	'# AB.Error.Raise 10001
	'------------------------------------------------------------------------------------------

	Public Property Get Style
		Style = s_style
	End Property

	Public Property Let Style(ByVal s)
		s_style = s
	End Property

	Public Sub [Set](ByVal Number, ByVal Description, ByVal Source)
		o_err("Number") = Number
		o_err("Description") = Description
		o_err("Source") = Source
	End Sub

	Public Sub Raise(ByVal n)
		GetErrData()
		If AB.C.IsNul(n) Then Exit Sub
		i_errNum = n
		If b_debug Then
			If Err Then Err.Clear()
			AB.C.Put ShowMsg(o_abe(n) & s_msg, 1)
		End If
		s_msg = ""
		On Error GoTo 0
	End Sub

	Public Sub [Throw](ByVal msg)
		GetErrData()
		If Left(msg,1) = ":" Then
			If o_abe.Exists(Mid(msg,2)) Then msg = o_abe(Mid(msg,2))
		End If
		AB.C.Print ShowMsg(msg,0)
		On Error GoTo 0
	End Sub

	Public Sub Show(ByVal code, ByVal msg)
		If b_debug Then
			AB.ShowErr code, msg
		End If
	End Sub

	Public Sub See()
		GetErrData()
		If b_debug Then
			AB.ShowErr "0x" & Hex(o_err("Number")), o_err("Source") & "<br />" & o_err("Description")
		End If
	End Sub

	Public Sub Defined()
		Dim key
		If AB.C.Has(o_abe) Then
			For Each key In o_abe
				AB.C.Print key & " : " & o_abe(key) & "<br />" & VbCrLf
			Next
		End If
	End Sub

	'-- 错误捕捉处理 b --

	Public Sub ClearErr()
		i_errCode = 0
		s_errMsg = ""
		o_err("Number") = 0
		o_err("Description") = ""
		o_err("Source") = ""
		Err.Clear
	End Sub

	Public Sub SetErr(ByVal code, ByVal msg)
		i_errCode = code
		s_errMsg = msg
		ClearErr()
	End Sub

	Public Sub CatchErr()
		GetErrData()
		i_errCode = o_err("Number")
		s_errMsg = "<b>错误代码：</b>0x" & Hex(o_err("Number")) & " <b>错误来源：</b>" & o_err("Source") & " <b>错误描述：</b>" & o_err("Description") & ""
		ClearErr()
	End Sub

	Public Function GetErr(ByVal p)
		Dim temp
		Select Case Lcase(p)
			Case "0", "code" : temp = i_errCode
			Case "1", "msg" : temp = s_errMsg
		End Select
		GetErr = temp
	End Function

	'-- 错误捕捉处理 e --

	Private Function ShowMsg(ByVal msg, ByVal t)
		Dim s,x,css,style
		style = AB.C.IfThen(AB.C.Has(s_style),"<style type=""text/css"">" & s_style & "</style>")
		css = AB.C.IfThen(AB.C.Has(s_css)," class=""" & s_css & """")
		If Trim(style) <> "" Then AB.C.Print style & "" & VbCrLf
		s = "<fieldset id=""abxError""" & css & ">" & vbCrLf
		s = s & "	<legend>" & s_title & "</legend>" & vbCrLf
		s = s & "	<p class=""msg"">" & msg & "</p>" & vbCrLf
		x = AB.C.IIF(s_url = "javascript:history.go(-1)", "返回", "继续")
		If t = 1 Then
			If o_err("Number")<>0 Then
				s = s & "	<ul class=""dev"">" & vbCrLf
				s = s & "		<li class=""info"">以下信息针对开发者：</li>" & vbCrLf
				s = s & "		<li>错误代码：0x" & Hex(o_err("Number")) & "</li>" & vbCrLf
				s = s & "		<li>错误描述：" & o_err("Description") & "</li>" & vbCrLf
				s = s & "		<li>错误来源：" & o_err("Source") & "</li>" & vbCrLf
				s = s & "	</ul>" & vbCrLf
			End If
		Else
			If b_redirect Then
				s = s & "	<p class=""back"">页面将在" & (i_delay/1000) & "秒钟后跳转，如果浏览器没有正常跳转，<a href=""" & s_url & """>请点击此处" & x & "</a></p>" & vbCrLf
				s_url = AB.C.IIF(Left(s_url,11) = "javascript:", Mid(s_url,12), "location.href='" & s_url & "';")
				s = s & "<script type='text/javascript'>setTimeout(function(){" & s_url & "}," & i_delay & ");</script>"
			Else
				s = s & "	<p class=""back""><a href=""" & s_url & """>请点击此处" & x & "</a></p>" & vbCrLf
			End If
		End If
		s = s & "</fieldset>" & vbCrLf
		ShowMsg = s
	End Function

	Private Sub GetErrData()
		o_err("Number") = AB.C.IIF(o_err("Number")=0, Err.Number, o_err("Number"))
		o_err("Description") = AB.C.IIF(o_err("Description")="", Err.Description, o_err("Description"))
		o_err("Source") = AB.C.IIF(o_err("Source")="", Err.Source, o_err("Source"))
	End Sub

End Class
%>