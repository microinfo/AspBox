<%
'######################################################################
'## ab.upload.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Upload Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2012/06/08 1:01
'## Description :   Upload file(s) with AspBox
'######################################################################

Dim Abx_o_updata
Class Cls_AB_Upload
	Public Form, File, Count
	Private s_charset,s_key,s_allowed,s_denied
	Private s_savePath,s_jsonPath,s_progressPath,s_progExt
	Private i_fileMaxSize,i_totalMaxSize,i_blockSize
	Private b_useProgress, b_autoMD,b_random, o_fso, o_db
	Private Sub Class_Initialize
		s_charset	= AB.CharSet
		s_key		= ""
		s_allowed	= ""
		s_denied	= ""
		s_savePath	= ""
		b_autoMD	= True
		b_random	= False
		i_fileMaxSize	= 0
		i_totalMaxSize = 0
		i_blockSize = 64 * 1024
		b_useProgress = False
		s_progressPath = "/__uptemp/"
		s_jsonPath	= ""
		s_progExt = ".txt"
		AB.Error(71) = "表单类型错误，表单只能是""multipart/form-data""类型！"
		AB.Error(72) = "请先选择要上传的文件！"
		AB.Error(73) = "上传文件失败，上传文件总大小超过了限制！"
		AB.Error(74) = "上传文件失败，上传文件不能为空！"
		AB.Error(75) = "上传文件失败，文件大小超过了限制！"
		AB.Error(76) = "上传文件失败，不允许上传此类型的文件！"
		AB.Error(77) = "上传文件失败！"
		AB.Error(78) = "获取文件失败！"
		AB.Error(79) = "本次上传KEY不能为空，否则上传进度条不可用！"
		AB.Error(70) = "保存进度条目录必须以 / 开头！"
		Set Form = Server.CreateObject(AB.dictName)
		Set File = Server.CreateObject(AB.dictName)
		Count = 0
		AB.Use "db"
		Set o_db = AB.db.New
		GetConn AB.db.Conn
		AB.Use "Form"
	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Upload.CharSet 属性(只写)
	'# @syntax: AB.Upload.CharSet = string
	'# @return: 无返回值
	'# @dowhat: 设置上传文件时的文件编码,此属性只写
	'#  Upload组件操作文件的文件字符集编码，如"UTF-8"或"GB2312"，应和网站编码一致，避免出现乱码的情况。
	'#  默认继承 AB.CharSet 的值。
	'--DESC------------------------------------------------------------------------------------
	'# @param string : String (字符串) 
	'#  代表字符集编码的字符串，如：
	'#  "BIG5" - 繁体中文
	'#  "GB2312" - 简体中文
	'#  "KOI8-R" - 俄语
	'#  "UTF-8" - ASCII兼容的多字节8编码
	'#  默认继承 AB.CharSet 的值。
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Upload"
	'# AB.Upload.CharSet = "UTF-8"
	'------------------------------------------------------------------------------------------

	Public Property Let CharSet(ByVal s)
		s_charset = UCase(s)
	End Property

	Public Property Let Conn(ByVal o)
		GetConn o
	End Property

	Public Property Get Conn
		If TypeName(o_db.Conn) = "Connection" Then
			Set Conn = o_db.Conn
		Else
			If IsObject(Conn) Then Set Conn = Nothing
			AB.Error.Raise 13
		End If
	End Property

	Private Sub GetConn(ByVal o)
		If TypeName(o) = "Connection" Then
			If o.State = 1 Then
				o_db.Conn = o
			Else
				AB.Error.Raise 13
			End If
		End If
	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Upload.Key 属性(只写)
	'# @syntax: AB.Upload.Key = String
	'# @return: 无返回值
	'# @dowhat: 设置一个字符串Key，该值用于标识被上传文件以便生成客户端进度条数据。
	'#  Key为字符串类型，不可为空，否则上传进度条不可用。程序会根据Key的值在临时目录下生成临时文件。
	'--DESC------------------------------------------------------------------------------------
	'# @param String : String (字符串) 字符串Key
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Upload.Key = AB.C.Get("json")
	'# AB.Upload.Key = "ABUP-20100802155444-DB8EB8D0AA62B15A"
	'------------------------------------------------------------------------------------------

	Public Property Let Key(ByVal s)
		If Not b_useProgress Then Exit Property
		If AB.C.IsNul(s) Then AB.Error.Raise 79 : Exit Property
		s_key = s
		s_jsonPath = absPath(s_progressPath) & s & s_progExt
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Upload.GenKey 属性(只读)
	'# @syntax: [strKey = ]AB.Upload.GenKey
	'# @return: String (字符串) 返回一个由三部分组成的字符串，分别是："ABUP-"、当前时间字符串 以及一个随机字符串。
	'# @dowhat: 生成本次上传的唯一KEY
	'#  该方法能够生成一个可以标识当前上传文件的长字符串，用户可以将此字符串作为参数传递给AB.Upload.Key作为上传标识。
	'--DESC------------------------------------------------------------------------------------
	'# @param: 无参数
	'--DEMO------------------------------------------------------------------------------------
	'# ABrandom = AB.Upload.GenKey
	'------------------------------------------------------------------------------------------

	Public Property Get GenKey
		GenKey = "AspBoxUP-" & AB.C.DateTime(Now,"ymmddhhiiss") & AB.C.RandStr("-<16>:0123456789ABCDEF")
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Upload.FileMaxSize 属性
	'# @syntax: AB.Upload.FileMaxSize[ = decimal]
	'# @return: Integer (数值) 取得设置的允许上传的单个文件最大值（单位KB）
	'# @dowhat: 允许上传的单个文件最大值，单位是KB
	'#  该属性设置每次上传所允许单个文件大小的最大值。
	'--DESC------------------------------------------------------------------------------------
	'# @param decimal : Numeric (数值) 十进制数值
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Upload"
	'# AB.Upload.FileMaxSize = 1024*5    '限制单个文件上传尺寸为5M
	'------------------------------------------------------------------------------------------

	Public Property Let FileMaxSize(ByVal n)
		i_fileMaxSize = n * 1024
	End Property

	Public Property Get FileMaxSize
		FileMaxSize = i_fileMaxSize / 1024
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Upload.TotalMaxSize 属性
	'# @syntax: AB.Upload.TotalMaxSize[ = decimal]
	'# @return: Integer (数值) 取得设置的允许总上传尺寸（单位KB）
	'# @dowhat: 设置允许总上传尺寸，单位是KB
	'#  本属性限制了每次上传的所有文件的总大小，善用此功能，
	'#  在多文件批量上传环境下，可以确保服务器的负载安全，又能兼顾单个文件上传尺寸的灵活设置。
	'--DESC------------------------------------------------------------------------------------
	'# @param decimal : Numeric (数值) 十进制数值
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Upload"
	'# AB.Upload.TotalMaxSize= 1024*100    '设置允许总上传尺寸为100M
	'------------------------------------------------------------------------------------------

	Public Property Let TotalMaxSize(ByVal n)
		i_totalMaxSize = n * 1024
	End Property

	Public Property Get TotalMaxSize
		TotalMaxSize = i_totalMaxSize / 1024
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Upload.Allowed 属性
	'# @syntax: AB.Upload.Allowed[ = string]
	'# @return: String (字符串) 取得设置的允许上传的文件类型
	'# @dowhat: 设置和获取允许上传的文件类型
	'#  本属性用以设置允许上传文件的类型，也就是俗称的白名单，多个类型用"|"分隔。
	'#  设置了本属性则Denied属性（即禁止上传的文件类型）无效。
	'--DESC------------------------------------------------------------------------------------
	'# @param string : String (字符串)
	'#  本参数为以"|"分隔的多个文件类型组合而成的连续字符串，如 "jpg|jpeg|bmp|png|gif" 
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Upload"
	'# AB.Upload.Allowed = "jpg|jpeg|bmp|png|gif"    '用于图片上传时的常用设置
	'------------------------------------------------------------------------------------------

	Public Property Let Allowed(ByVal s)
		s_allowed = s
	End Property

	Public Property Get Allowed
		Allowed = s_allowed
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Upload.Denied 属性
	'# @syntax: AB.Upload.Denied[ = string]
	'# @return: String (字符串) 取得设置的禁止上传的文件类型
	'# @dowhat: 设置和获取禁止上传的文件类型
	'#  本属性设置禁止上传的文件类型，也就是俗称的黑名单，多个类型用"|"分隔。
	'#  本属性与Allowed属性正好相反，当接收环境不好确定时，可以设置本属性以限制不安全的上传类型，
	'#  当设置了Allowed属性后本属性无效。
	'--DESC------------------------------------------------------------------------------------
	'# @param string : String (字符串) 
	'#  本参数为以"|"分隔的多个文件类型组合而成的连续字符串，如 "jpg|jpeg|bmp|png|gif" 
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Upload"
	'# AB.Upload.Allowed = "exe|asp|asa|bat"    '限制不安全上传类型的常用设置
	'------------------------------------------------------------------------------------------

	Public Property Let Denied(ByVal s)
		s_denied = s
	End Property

	Public Property Get Denied
		Denied = s_denied
	End Property

	Public Property Let SavePath(ByVal s)
		Dim Matches,Match,t
		If AB.C.Test(s,"<.+?>") Then
			Set Matches = AB.C.RegMatch(s,"<(.+?)>")
			For Each Match In Matches
				t = AB.C.DateTime(Now,Match.SubMatches(0))
				s = Replace(s,Match.Value,t)
			Next
		End If
		If Not Instr(s,":") = 2 Then
			If Right(s,1) <> "/" Then s = s & "/"
		Else
			If Right(s,1) <> "\" Then s = s & "\"
		End If
		s_savepath = s
	End Property

	Public Property Get SavePath
		SavePath = absPath(s_savepath)
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Upload.UseProgress 属性
	'# @syntax: AB.Upload.UseProgress = Boolean
	'# @return: 无返回值
	'# @dowhat: 设置是否使用进度条，默认不使用
	'#  本属性设置是否使用进度条，需开启时设置为true。
	'--DESC------------------------------------------------------------------------------------
	'# @param Boolean : Boolean (布尔值) 可用值为true和false。 
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Upload"
	'# AB.Upload.UseProgress = True    '设置开启进度条功能
	'------------------------------------------------------------------------------------------

	Public Property Let UseProgress(ByVal b)
		b_useProgress = b
		If b Then 
			AB.Use "Fso"
			Set o_fso = New Cls_AB_Fso
		End If
	End Property

	Public Property Let ProgressPath(ByVal s)
		If AB.C.IsNul(s) Then Exit Property
		If Left(s,1)<>"/" Then AB.Error.Raise 70 : Exit Property
		If Right(s,1)<>"/" Then s = s & "/"
		s_progressPath = s
		If AB.C.Has(s_key) Then s_jsonPath = absPath(s_progressPath) & s_key & s_progExt
	End Property

	Public Property Get ProgressPath
		ProgressPath = s_progressPath
	End Property

	Public Function ProgressFile(ByVal key)
		If AB.C.Has(key) Then
			ProgressFile = s_progressPath & key & s_progExt
		End If
	End Function

	Public Property Let AutoMD(ByVal b)
		b_autoMD = b
	End Property

	Public Property Let Random(ByVal b)
		b_random = b
	End Property

	Public Property Let BlockSize(ByVal i)
		i_blockSize = Int(i) * 1024
	End Property

	Private Function absPath(ByVal s)
		If AB.C.IsNul(s) Then s = "."
		s = AB.C.IIF(Instr(s,":")=2, s, Server.MapPath(s))
		If Right(s,1)<>"\" Then s = s & "\"
		absPath = s
	End Function

	Public Function checkFileType(ByVal t)
		checkFileType = True
		If AB.C.Has(s_allowed) Then
			If Not AB.C.Test(t, "^" & s_allowed & "$") Then
				checkFileType = False
				Exit Function
			End If
		ElseIf AB.C.Has(s_denied) Then
			If AB.C.Test(t,"^" & s_denied & "$") Then
				checkFileType = False
				Exit Function
			End If
		End If
	End Function

	Public Sub StartUpload
		Dim ContentType
		If TypeName(o_db.Conn) <> "Connection" Then
				GetConn AB.db.Conn
		End If
		ContentType = Request.ServerVariables("HTTP_CONTENT_TYPE")
		If Trim(ContentType)="" Then ContentType = Request.ServerVariables("CONTENT_TYPE")
		AB.Form.Up = "upload"
		Dim FormType : FormType = Split(ContentType, ";")
		If LCase(FormType(0)) <> "multipart/form-data" Then
			AB.Error.Raise 71
			Exit Sub
		End If
		Dim o_strm, o_prog, o_file
		Dim s_block, s_blockData, s_start, s_formName, s_formValue, s_fileName, s_data
		Dim i_total, i_loaded, i_block, i_formStart, i_formEnd, i_Start, i_End, i_dataStart, i_dataEnd
		Dim CrLf
		i_total = Request.TotalBytes
		If i_total < 1 Then AB.Error.Raise 72 : Exit Sub
		Set o_strm = Server.CreateObject(AB.steamName)
		Set Abx_o_updata = Server.CreateObject(AB.steamName)
		Abx_o_updata.Type = 1
		Abx_o_updata.Mode =3
		Abx_o_updata.Open
		i_loaded = 0
		If b_useProgress Then
			AB.Use "Fso"
			If Not AB.Fso.IsFolder(s_progressPath) Then
				AB.Fso.MD s_progressPath
			End If
			Set o_prog = New Cls_AB_Upload_Progress
			o_prog.TotalSize = i_total
			o_prog.Create(s_jsonPath)
		End If
		Do While i_loaded < i_total
			i_block = i_blockSize
			If i_block + i_loaded > i_total Then i_block = i_total - i_loaded
			s_block = Request.BinaryRead(i_block)
			i_loaded = i_loaded + i_block
			Abx_o_updata.Write s_block
			If b_useProgress Then o_prog.Update(i_loaded)
		Loop
		Abx_o_updata.Position = 0
		s_blockData = Abx_o_updata.Read
		i_formStart = 1
		i_formEnd = LenB(s_blockData)
		CrLf = chrB(13) & chrB(10)
		s_start = MidB(s_blockData,1, InStrB(i_formStart,s_blockData,CrLf)-1)
		i_start = LenB(s_start)
		i_formStart = i_formStart + i_start + 1
		While (i_formStart + 10) < i_formEnd 
			i_End = InStrB(i_formStart,s_blockData,CrLf & CrLf)+3
			o_strm.Type = 1
			o_strm.Mode =3
			o_strm.Open
			Abx_o_updata.Position = i_formStart
			Abx_o_updata.CopyTo o_strm, i_End-i_formStart
			o_strm.Position = 0
			o_strm.Type = 2
			o_strm.Charset = s_charset
			s_data = o_strm.ReadText
			o_strm.Close
			i_formStart = InStrB(i_End,s_blockData,s_start)
			i_dataStart = InStr(22,s_data,"name=""",1) + 6
			i_dataEnd = InStr(i_dataStart,s_data,"""",1)
			s_formName = Mid(s_data,i_dataStart,i_dataEnd-i_dataStart)
			If InStr(43,s_data,"filename=""",1) > 0 Then
				Set o_file = New Cls_AB_Upload_FileInfo
				o_file.autoMD = b_autoMD
				o_file.Size = i_formStart - i_End - 3
				If (i_fileMaxSize>0 And (o_file.Size)>i_fileMaxSize) Then
					o_file.isSize = False
					AB.Error.Raise 75
				End If
				If o_file.Size > 0 Then
					i_dataStart = InStr(i_dataEnd,s_data,"filename=""",1) + 10
					i_dataEnd = InStr(i_dataStart,s_data,"""",1)
					s_fileName = Mid(s_data,i_dataStart,i_dataEnd-i_dataStart)
					o_file.Client = s_fileName
					o_file.OldPath = Left(s_fileName, InstrRev(s_fileName, "\"))
					o_file.NewPath = absPath(s_savepath)
					o_file.WebPath = s_savepath
					o_file.Name = Mid(s_fileName, InstrRev(s_fileName, "\")+1)
					o_file.Ext = Mid(o_file.Name, InstrRev(o_file.Name,".")+1)
					o_file.NewName = AB.C.IIF(b_random,AB.C.DateTime(Now,"ymmddhhiiss")&AB.C.RandStr("<100000-999999>") & "." & o_file.Ext,o_file.Name)
					If Not checkFileType(o_file.Ext) Then
						o_file.isType = False
						AB.Error.Raise 76
					End If
					i_dataStart = InStr(i_dataEnd,s_data,"Content-Type: ",1) + 14
					i_dataEnd = InStr(i_dataStart,s_data,vbCr)
					o_file.MIME = Mid(s_data,i_dataStart,i_dataEnd-i_dataStart)
					o_file.Start = i_End
					o_file.FormName = s_formName
					If o_file.isSize And o_file.isType Then
						Count = Count + 1
					End If
				End If
				If NOT File.Exists(s_formName) Then
					File.Add s_formName, o_file
				End If
				Set o_file = Nothing
			Else
				o_strm.Type = 1
				o_strm.Mode = 3
				o_strm.Open
				Abx_o_updata.Position = i_End 
				Abx_o_updata.CopyTo o_strm, i_formStart-i_End-3
				o_strm.Position = 0
				o_strm.Type = 2
				o_strm.Charset = s_charset
				s_formValue = o_strm.ReadText
				o_strm.Close
				If Form.Exists(s_formName) Then
					Form(s_formName) = Form(s_formName) & ", " & s_formValue
				Else
					Form.Add s_formName, s_formValue
				End If
			End If
			i_formStart = i_formStart + i_start + 1
		Wend
		s_blockData = ""
		Set o_strm = Nothing
		Set o_prog = Nothing
	End Sub

	Public Sub SaveAll
		Dim f
		If AB.C.Has(File) Then
			For Each f In File
				File(f).Save
			Next
		End If
	End Sub

  Private Sub Class_Terminate  
		If Request.TotalBytes > 0 Then
			Form.RemoveAll
			File.RemoveAll
			AB.Free(Abx_o_updata)
		End If
		Set Form = Nothing
		Set File = Nothing
		Set o_db = Nothing
		If b_useProgress Then
			If o_fso.IsFile(s_jsonPath) Then o_fso.DelFile s_jsonPath
			Set o_fso = Nothing
		End If
  End Sub
End Class

Class Cls_AB_Upload_FileInfo
	Public FormName, Client, OldPath, NewPath, WebPath, Name, NewName, Ext, Size, MIME
	Public isSize, isType, autoMD, Start
	Private Sub Class_Initialize
		FormName = ""
		Client = ""
		OldPath = ""
		NewPath = ""
		WebPath = ""
		Name = ""
		NewName = ""
		Ext = ""
		Size = 0
		Start = 0
		MIME = ""
		isSize = True
		isType = True
		autoMD = True
	End Sub

	Public Function SaveAs(ByVal p)
		Dim o_strm,s_path
		SaveAs = True
		If Size <= 0 Then
			SaveAs = False
			Exit Function
		ElseIf Not isSize Then
			SaveAs = False
			AB.Error.Raise 75
			Exit Function
		End If
		If AB.C.IsNul(p) Or AB.C.IsNul(Name) Or Start = 0 Or Right(p,1)="/" Then
			SaveAs = False
			Exit Function
		End If
		If Not isType Then
			SaveAs = False
			AB.Error.Raise 76
			Exit Function
		End If
		If autoMD Then
			AB.Use "Fso"
			s_path = Left(p,InstrRev(p,"\"))
			If Not AB.Fso.IsFolder(s_path) Then AB.Fso.MD(s_path)
		End If
		Set o_strm = Server.CreateObject(AB.steamName)
		o_strm.Mode = 3
		o_strm.Type = 1
		o_strm.Open
		Abx_o_updata.position = Start
		Abx_o_updata.copyto o_strm, Size
		o_strm.SaveToFile p, 2
		o_strm.Close
		Set o_strm = Nothing
	End Function

	Public Function Save
		Save = SaveAs(NewPath & NewName)
	End Function

End Class
Class Cls_AB_Upload_Progress
	Private s_path,i_total
	Private o_json,o_timer
	Private Sub Class_Initialize
		AB.Use "Fso"
		AB.Fso.OverWrite = True
		i_total = 0
		s_path = ""
	End Sub

	Private Sub Class_Terminate
		If TypeName(o_json)="Cls_AB_JSON" Then Set o_json = Nothing
	End Sub

	Public Property Let TotalSize(ByVal i)
		i_total = i
	End Property

	Public Sub Create(ByVal p)
		s_path = p
		o_timer = Timer
		AB.Use "Json"
		Set o_json = AB.Json.New(0)
		o_json("total") 	= AB.Fso.FormatSize(i_total,"AUTO")
		o_json("uploaded")	= "0 KB"
		o_json("percent")	= "0"
		o_json("speed") 	= "0 KB"
		o_json("passtime") 	= "00:00:00"
		o_json("totaltime")	= "00:00:00"
		o_json("uploadtime")= AB.C.DateTime(Now(),"y-mm-dd hh:ii:ss")
		Call AB.Fso.CreateFile(s_path, o_json.jsString)
	End Sub

	Sub Update(ByVal loaded)
		Dim speed,cTimer,totalTime,remainTime,percent
		speed = 0.0001
		cTimer = Timer
		If (cTimer - o_timer)>0 Then speed = loaded / (cTimer - o_timer)
		totalTime = i_total / speed
		remainTime = (i_total - loaded) / speed
		percent = Round(loaded *100 / i_total,1)
		AB.Use "Json"
		o_json("uploaded")	= AB.Fso.FormatSize(loaded,"AUTO")
		o_json("percent")	= percent
		o_json("speed") 	= AB.Fso.FormatSize(speed,"AUTO") & "/S" 
		o_json("totaltime") = SecToTime(totalTime)
		o_json("remaintime")= SecToTime(remainTime)
		o_json("uploadtime")= AB.C.DateTime(Now(),"y-mm-dd hh:ii:ss")
		Call AB.Fso.CreateFile(s_path, o_json.jsString)       
	End Sub

	private Function SecToTime(ByVal sec)
		On Error Resume Next
		Dim h : h = "00"
		Dim m : m = "00"
		Dim s : s = "00"
		If isNumeric(sec) Then
			h = Right("0" & Round(sec/3600), 2)
			m = Right("0" & Round(mod_(sec,3600) / 60), 2)
			s = Right("0" & Round(mod_(sec,60)), 2)
		End If
		SecToTime = (h & ":" & m & ":" & s)
		On Error Goto 0
	End Function

	Private Function mod_(ByVal a, ByVal b)
		mod_ = (a - int(a/b)*b)
	End Function

End Class
%>