<%
'######################################################################
'## ab.fso.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox FileSystemObject Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/09/27 22:42
'## Description :   AspBox Files System Operator
'######################################################################

Class Cls_AB_Fso
	Public oFso, IsVirtualHost
	Private Fso, o_imgwh
	Private b_force,b_overwrite,b_init
	Private s_fsoName,s_steamName,s_sizeformat,s_charset
	Private i_list, o_flist

	Private Sub Class_Initialize
		s_fsoName 	= AB.FsoName
		s_steamName	= AB.steamName
		s_charset	= AB.CharSet
		Set Fso 	= Server.CreateObject(s_fsoName)
		Set oFso 	= Fso
		Set o_imgwh = New ImgWHInfo
		IsVirtualHost = True '设置服务器是否是虚拟主机（在遇到权限问题时可尝试修改此属性）
		b_force		= True
		b_overwrite	= True
		b_init		= False
		s_sizeformat= "K"
		AB.Error(52) = "写入文件错误！"
		AB.Error(53) = "创建文件夹错误！"
		AB.Error(54) = "读取文件列表失败！"
		AB.Error(55) = "设置属性失败，文件不存在！"
		AB.Error(56) = "设置属性失败！"
		AB.Error(57) = "获取属性失败，文件不存在！"
		AB.Error(58) = "复制失败，源文件不存在！"
		AB.Error(59) = "移动失败，源文件不存在！"
		AB.Error(60) = "删除失败，文件不存在！"
		AB.Error(61) = "重命名失败，源文件不存在！"
		AB.Error(62) = "重命名失败，已存在同名文件！"
		AB.Error(63) = "文件或文件夹操作错误！"
		AB.Error(64) = "文件夹不存在！"
	End Sub

	Private Sub Class_Terminate
		Set Fso 	= Nothing
		Set oFso 	= Nothing
		Set o_imgwh = Nothing
	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.Fso.New 方法
	'# @syntax: Set f = AB.Fso.New
	'# @return: Object (ASP对象)
	'# @dowhat: 创建Fso核心对象, 创建一个 AspBox_Fso 对象
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'# Dim f : Set f = AB.Fso.New 	'创建Fso操作对象
	'------------------------------------------------------------------------------------------

	Public Function [New]()
		Set [New] = New Cls_AB_Fso
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.FsoName 属性
	'# @syntax: AB.Fso.FsoName = string
	'# @return: 无返回值
	'# @dowhat: 设置服务器FSO组件的名称，此属性只写
	'#  一般来说，服务器上FSO组件的名称为“Scripting.FilesyStemObject”。
	'#  如果服务器上的FSO组件修改了名称，可以用此属性设置服务器上FSO组件的名称。
	'#  如果没有修改，则可忽略此属性。
	'#  此属性主要用于AspBox的服务器文件相关操作，如 AB.C.Include 方法。
	'#  P.S. 服务器为了增强安全性，可通过修改注册表 HKEY_CLASSES_ROOT\Scripting.FileSystemObject 的键值来修改FSO组件的名称。
	'--DESC------------------------------------------------------------------------------------
	'# @param string: String (字符串) 服务器FSO组件的名称，默认为“Scripting.FilesyStemObject”
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# AB.Fso.FsoName = "Scripting.FilesyStemObject"
	'------------------------------------------------------------------------------------------

	Public Property Let fsoName(Byval str)
		s_fsoName = str
		Set Fso = Server.CreateObject(s_fsoName)
		Set oFso = Fso
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Fso.SteamName 属性
	'# @syntax: AB.Fso.SteamName = string
	'# @return: 无返回值
	'# @dowhat: 设置服务器ADODB.Stream组件的名称，此属性只写
	'--DESC------------------------------------------------------------------------------------
	'# @param string: String (字符串) 服务器ADODB.Stream组件的名称，默认为“ADODB.Stream”
	'--DEMO------------------------------------------------------------------------------------
	'# none
	'------------------------------------------------------------------------------------------

	Public Property Let SteamName(Byval str)
		s_steamName = str
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Fso.CharSet 属性
	'# @syntax: AB.Fso.CharSet = charset
	'# @return: 无返回值
	'# @dowhat: 设置服务器端操作文件的文件编码，此属性只写
	'# 			FSO组件操作文件的文件字符集编码，如"UTF-8"或"GB2312"，避免出现乱码的情况。
	'# 			默认为 AB.CharSet 的值。
	'--DESC------------------------------------------------------------------------------------
	'# @param: none
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# AB.Fso.CharSet = "UTF-8"
	'------------------------------------------------------------------------------------------

	Public Property Let CharSet(Byval str)
		s_charset = Ucase(str)
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Fso.Force 属性
	'# @syntax: AB.Fso.Force = boolean
	'# @return: 无返回值
	'# @dowhat: 当此属性设置为真时，可删除服务器上的只读文件
	'--DESC------------------------------------------------------------------------------------
	'# @param boolean: Boolean (布尔值) 为真则表示可删除服务器上的只读文件，为假则不删除。默认为 True
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# AB.Fso.Force = False
	'------------------------------------------------------------------------------------------

	Public Property Let Force(Byval bool)
		b_force = bool
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Fso.OverWrite 属性
	'# @syntax: AB.Fso.OverWrite = boolean
	'# @return: 无返回值
	'# @dowhat: 当此属性设置为真时，将覆盖服务器端原有的同名文件
	'--DESC------------------------------------------------------------------------------------
	'# @param boolean: Boolean (布尔值) 为真则表示进行文件操作时将覆盖服务器端原有的同名文件，为假则不覆盖。默认为 True
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# AB.Fso.OverWrite = False
	'------------------------------------------------------------------------------------------

	Public Property Let OverWrite(Byval bool)
		b_overwrite = bool
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Fso.SizeFormat 属性
	'# @syntax: AB.Fso.SizeFormat = string
	'# @return: 无返回值
	'# @dowhat: 设置文件大小显示格式，此属性只写
	'--DESC------------------------------------------------------------------------------------
	'# @param string: String (字符串) 可选值有：G,M,K,b,auto，默认值为 K
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# AB.Fso.SizeFormat = "M"
	'------------------------------------------------------------------------------------------

	Public Property Let SizeFormat(Byval str)
		s_sizeformat = str
	End Property

	Public Property Get ShowErr()
		ShowErr = s_err
	End Property

	'------------------------------------------------------------------------------------------
	'# AB.Fso.isExists(path) 判断文件或文件夹是否存在
	'# @return: Boolean (布尔值) 文件或文件夹存在返回真（True），不存在则返回假（False）
	'# @dowhat: 判断文件或文件夹是否存在
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串)
	'# 文件或文件夹的路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# If AB.Fso.IsExists("/Test.html") Then
	'# 	AB.C.Print "Test.html存在"
	'# Else
	'# 	AB.C.Print "Test.html不存在"
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function isExists(ByVal path)
		isExists = False
		If isFile(path) or isFolder(path) Then isExists = True
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.IsFile(filePath) 判断文件是否存在
	'# @return: Boolean (布尔值) 文件存在返回真（True），不存在则返回假（False）
	'# @dowhat: 判断文件是否存在
	'--DESC------------------------------------------------------------------------------------
	'# @param filePath: [String] (字符串)
	'# 文件路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# If AB.Fso.IsFile("/Test.html") Then
	'# 	AB.C.Print "Test.html存在"
	'# Else
	'# 	AB.C.Print "Test.html不存在"
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function isFile(ByVal filePath)
		filePath = absPath(filePath) : isFile = False
		If Fso.FileExists(filePath) Then isFile = True
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.Read(filePath) 读取服务器端文件内容
	'# @return: String (字符串) 返回读取到的文件内容（字符串）
	'# @dowhat: 调用此方法可获取服务器上指定文件的内容，并返回该内容字符串
	'--DESC------------------------------------------------------------------------------------
	'# @param filePath: [String] (字符串)
	'# 要读取文件的路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim TempStr
	'# TempStr = AB.Fso.Read("/Test.html")
	'# AB.C.Print TempStr
	'------------------------------------------------------------------------------------------

	Public Function Read(ByVal filePath)
		Dim p, f, o_strm, tmpStr, s_char
		s_char = s_charset
		If Instr(filePath,">")>0 Then
			s_char = UCase(Trim(AB.C.CRight(filePath,">")))
			filePath = Trim(AB.C.CLeft(filePath,">"))
		End If
		p = absPath(filePath)
		If isFile(p) Then
			Set o_strm = Server.CreateObject(s_steamName)
			With o_strm
				.Type = 2
				.Mode = 3
				.Open
				.LoadFromFile p
				.Charset = s_char
				.Position = 2
				tmpStr = .ReadText
				.Close
			End With
			Set o_strm = Nothing
			If s_char = "UTF-8" Then
				Select Case AB.FileBOM
					Case "keep"
						'Do Nothing
					Case "remove"
						If AB.C.Test(tmpStr, "^\uFEFF") Then
							tmpStr = AB.C.RegReplace(tmpStr, "^\uFEFF", "")
						End If
					Case "add"
						If Not AB.C.Test(tmpStr, "^\uFEFF") Then
							tmpStr = Chrw(&hFEFF) & tmpStr
						End If
				End Select
			End If
		Else
			tmpStr = ""
			AB.Error.Msg = "(" & filePath & ")"
			AB.Error.Raise 2
		End If
		Read = tmpStr
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.ReadBin(filePath) 以二进制形式读取服务器端文件内容
	'# @return: String (字符串) 返回读取到的文件内容（二进制形式）
	'# @dowhat: 调用此方法可获取服务器上指定文件的二进制形式内容，并返回该二进制形式内容
	'--DESC------------------------------------------------------------------------------------
	'# @param filePath: [String] (字符串)
	'# 要读取文件的路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim steam
	'# steam = AB.Fso.ReadBin("/aaa.jpg")
	'# 'Call AB.Fso.SaveAs("/bbb.jpg",steam) '保存(复制)到另一个文件
	'# Response.Clear
	'# Response.BinaryWrite steam '输出二进制流(可用来读取图片数据)
	'# Response.End
	'------------------------------------------------------------------------------------------

	Public Function ReadBin(ByVal filePath)
		Dim p, f, o_strm, tmpStr, s_char
		s_char = s_charset
		If Instr(filePath,">")>0 Then
			s_char = UCase(Trim(AB.C.CRight(filePath,">")))
			filePath = Trim(AB.C.CLeft(filePath,">"))
		End If
		p = absPath(filePath)
		If isFile(p) Then
			Set o_strm = Server.CreateObject(s_steamName)
			With o_strm
				.Type = 1 '以二进制模式(adTypeBinary)打开 
				.Open
				.LoadFromFile p
				tmpStr = .Read
				.Close
			End With
			Set o_strm = Nothing
		Else
			tmpStr = ""
			AB.Error.Msg = "(" & filePath & ")"
			AB.Error.Raise 2
		End If
		ReadBin = tmpStr
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.SaveAs(filePath, fileContent) 以二进制形式保存文件内容，不存在则创建
	'# @return: Boolean (布尔值) 保存文件成功返回真（True），失败则返回假（False）
	'# @dowhat: 调用此方法可向服务器上指定文件写入内容，文件不存在则创建
	'--DESC------------------------------------------------------------------------------------
	'# @param filePath: [String] (字符串)
	'# 要读取文件的路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'# @param fileContent: [String] (字符串)
	'# 写入的文件内容
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim steam
	'# steam = AB.Fso.ReadBin("/aaa.jpg")
	'# If AB.Fso.SaveAs("/bbb.jpg",steam) Then
	'# 	AB.C.Print "保存成功"
	'# Else
	'# 	AB.C.Print "保存失败"
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function SaveAs(ByVal filePath, ByVal fileContent)
		On Error Resume Next
		Dim f,p,t, o_strm
		p = absPath(filePath)
		SaveAs = MD(Left(p,InstrRev(p,"\")-1))
		If SaveAs Then
			Set o_strm = Server.CreateObject(s_steamName)
			With o_strm
				.Type = 1 '以二进制模式(adTypeBinary)打开 
				.Open
				.Write fileContent
				.SaveToFile p,AB.C.IIF(b_overwrite,2,1)
				.Close()
			End With
			Set o_strm = Nothing
		End If
		If Err.Number<>0 Then
			SaveAs = False
			AB.Error.Msg = "(" & filePath & ")"
			AB.Error.Raise 52
		End If
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.CreateFile(filePath, fileContent) 在服务器端创建文件并写入内容
	'# @return: Boolean (布尔值) 文件创建成功返回真（True），失败返回假（False）
	'# @dowhat: 该方法可将任意字符串写入文件并保存在服务器上
	'--DESC------------------------------------------------------------------------------------
	'# @param filePath: [String] (字符串) 文件保存的路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'# @param fileContent: [String] (字符串) 文件的内容，可以是任意字符串。
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Content,Result
	'# Content = "测试字符串"
	'# Result = AB.Fso.CreateFile("/Test.txt",Content)
	'# If Result Then
	'#     AB.C.Print "文件创建成功"
	'# Else
	'#     AB.C.Print "文件创建失败"
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function CreateFile(ByVal filePath, ByVal fileContent)
		On Error Resume Next
		Dim f,p,t, s_char, o_strm
		s_char = s_charset
		If Instr(filePath,">")>0 Then
			s_char = UCase(Trim(AB.C.CRight(filePath,">")))
			filePath = Trim(AB.C.CLeft(filePath,">"))
		End If
		p = absPath(filePath)
		CreateFile = MD(Left(p,InstrRev(p,"\")-1))
		If CreateFile Then
			Set o_strm = Server.CreateObject(s_steamName)
			With o_strm
				.Type = 2
				.Open
				.Charset = s_char
				.Position = o_strm.Size
				.WriteText = fileContent
				.SaveToFile p,AB.C.IIF(b_overwrite,2,1)
				.Close
			End With
			Set o_strm = Nothing
		End If
		If Err.Number<>0 Then
			CreateFile = False
			AB.Error.Msg = "(" & filePath & ")"
			AB.Error.Raise 52
		End If
		On Error Goto 0
	End Function
	
	'------------------------------------------------------------------------------------------
	'# AB.Fso.UpdateFile(filePath, rule, result) 按正则表达式更新文件内容
	'# @return: Boolean (布尔值) 更新成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法将把指定文件（filePath）中的匹配rule参数中正则表达式规则的字符串替换为参数result中的值
	'--DESC------------------------------------------------------------------------------------
	'# @param filePath: [String] (字符串)
	'# 要更新的文件的路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头) 
	'# @param rule: [String] (字符串) 待替换的正则表达式规则 
	'# @param result: [String] (字符串) 需要替换为的结果 
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '将Test.txt中的AspBox或ABox替换为AspBox v1.0
	'# Result = AB.Fso.UpdateFile("/Test.txt","A(sp)?[Bb]ox","AspBox v1.0")
	'# If Result Then
	'# 	AB.C.Print "文件更新成功"
	'# Else
	'# 	AB.C.Print "文件更新失败"
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function UpdateFile(ByVal filePath, ByVal rule, ByVal result)
		Dim tmpStr : filePath = absPath(filePath)
		tmpStr = AB.C.regReplace(Read(filePath),rule,result)
		UpdateFile = CreateFile(filePath,tmpStr)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.AppendFile(filePath, fileContent) 追加文件的内容
	'# @return: Boolean (布尔值) 追加成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法将在原文件（filePath）内容的末尾追加参数fileContent的内容。
	'--DESC------------------------------------------------------------------------------------
	'# @param filePath: [String] (字符串) 文件的路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'# @param fileContent: [String] (字符串) 要追加的内容，可以是任意字符串。
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Content,Result
	'# Content = "测试字符串"
	'# Result = AB.Fso.AppendFile("/Test.txt",Content)'将"测试字符串"追加到文件Test.txt中
	'# If Result Then
	'# 	AB.C.Print "文件内容追加成功"
	'# Else
	'# 	AB.C.Print "文件内容追加失败"
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function AppendFile(ByVal filePath, ByVal fileContent)
		Dim tmpStr : filePath = absPath(filePath)
		tmpStr = Read(filePath) & fileContent
		AppendFile = CreateFile(filePath,tmpStr)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.IsFolder(folderPath) 判断服务器上的文件夹是否存在
	'# @return: Boolean (布尔值) 文件夹存在返回真（True），不存在返回假（False）
	'# @dowhat: 调用此方法可判断服务器上的文件夹是否存在
	'--DESC------------------------------------------------------------------------------------
	'# @param folderPath: [String] (字符串)
	'# 文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# If AB.Fso.IsFolder("/Test/") Then
	'# 	AB.C.Print "Test文件夹存在"
	'# Else
	'# 	AB.C.Print "Test文件夹不存在"
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function isFolder(ByVal folderPath)
		folderPath = absPath(folderPath) : isFolder = False
		If Fso.FolderExists(folderPath) Then isFolder = True
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.CreateFolder(folderPath) 在服务器上创建文件夹
	'# @return: Boolean (布尔值) 文件夹创建成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法可在服务器上创建文件夹，不存在的上级文件夹会自动创建
	'--DESC------------------------------------------------------------------------------------
	'# @param folderPath: [String] (字符串) 文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# Result = AB.Fso.CreateFolder("/Test/1/2/3/")
	'# 'Result = AB.Fso.MD("/Test/1/2/3/") '简写
	'# If Result Then
	'# 	AB.C.Print "文件夹创建成功"
	'# Else
	'# 	AB.C.Print "文件夹创建失败"
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function CreateFolder(ByVal folderPath)
		On Error Resume Next
		Dim p,arrP,i : CreateFolder = True
		p = absPath(folderPath)
		arrP = Split(p,"\") : p = ""
		For i = 0 To Ubound(arrP)
			p = p & arrP(i) & "\"
			If IsVirtualHost Then
				If Instr(p, absPath("/") & "\")>0 Then
					If Not isFolder(p) And i>0 Then Fso.CreateFolder(p)
				End If
			Else
				If Not isFolder(p) And i>0 Then Fso.CreateFolder(p)
			End If
		Next
		If Err.Number<>0 Then
			CreateFolder = False
			AB.Error.Msg = "(" & folderPath & ")"
			AB.Error.Raise 53
		End If
		On Error Goto 0
	End Function

	Public Function MD(ByVal folderPath)
		MD = CreateFolder(folderPath)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.Dir(folderPath) 列出服务器上指定目录下所有的文件和文件夹
	'# @return: Array (数组)
	'# 			返回一个包含文件夹/文件属性的二维数组
	'# 			返回的属性包括：·名称 ·大小 ·最后更新时间 ·文件属性（文件夹无此值） ·类型
	'# @dowhat: 列出服务器上指定目录下所有的文件和文件夹
	'--DESC------------------------------------------------------------------------------------
	'# @param folderPath: [String] (字符串)
	'# 文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim P,Flist,i : P = "/"
	'# Flist = AB.Fso.Dir(P)
	'# 'AB.Trace Flist
	'# For i = 0 To UBound(Flist,2)
	'# 	AB.C.PrintCn "路径：" & P & Flist(0,i) &_
	'# 	" 大小：" & Flist(1,i) &_
	'# 	" 最后更新：" & Flist(2,i) &_
	'# 	" 属性：" & Flist(3,i) &_
	'# 	" 类型：" & Flist(4,i)
	'# Next
	'------------------------------------------------------------------------------------------

	Public Function Dir(ByVal folderPath)
		Dir = List(folderPath,0)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.List(folderPath, fileType) 列出服务器上指定目录下所有的文件或文件夹
	'# @return: Array (数组)
	'# 			返回一个包含文件夹/文件属性的二维数组
	'# 			返回的属性包括：·名称 ·大小 ·最后更新时间 ·文件属性（文件夹无此值） ·类型
	'# @dowhat: 列出服务器上指定目录下所有的文件和文件夹
	'--DESC------------------------------------------------------------------------------------
	'# @param folderPath: [String] (字符串)
	'# 文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'# @param fileType: [String] (字符串)
	'#  输出类型，可选值有：
	'#   0 或 ""，同时列出文件夹和文件；
	'#   1 或 "file"，只列出文件；
	'#   2 或 "folder"，只列出文件夹。
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim P,Flist,i : P = "/"
	'# Flist = AB.Fso.List(P,0) '同时列出文件夹和文件，也可写为：AB.Fso.List(P,"")
	'# 'Flist = AB.Fso.List(P,1) '只列出文件，也可写为：AB.Fso.List(P,"file")
	'# 'Flist = AB.Fso.List(P,2) '只列出文件夹，也可写为：AB.Fso.List(P,"folder")
	'# For i = 0 To UBound(Flist,2)
	'# 	AB.C.PrintCn "路径：" & P & Flist(0,i) &_
	'# 	" 大小：" & Flist(1,i) &_
	'# 	" 最后更新：" & Flist(2,i) &_
	'# 	" 属性：" & Flist(3,i) &_
	'# 	" 类型：" & Flist(4,i)
	'# Next
	'------------------------------------------------------------------------------------------

	Public Function List(ByVal folderPath, ByVal fileType)
		On Error Resume Next
		Dim f,fs,k,arr(),i,l
		folderPath = absPath(folderPath) : i = 0
		If Not isFolder(folderPath) Then
			AB.Error.Msg = "(" & folderPath & ")"
			AB.Error.Raise 64
			Exit Function
		End If
		Select Case LCase(fileType)
			Case "0","" l = 0
			Case "1","file" l = 1
			Case "2","folder" l = 2
			Case Else l = 0
		End Select
		Set f = Fso.GetFolder(folderPath)
		If l = 0 Or l = 2 Then
			Set fs = f.SubFolders
			ReDim Preserve arr(4,fs.Count-1)
			For Each k In fs
				arr(0,i) = k.Name & "/"
				arr(1,i) = formatSize(k.Size,s_sizeformat)
				arr(2,i) = k.DateLastModified
				arr(3,i) = Attr2Str(k.Attributes)
				arr(4,i) = k.Type
				i = i + 1
			Next
		End If
		If l = 0 Or l = 1 Then
			Set fs = f.Files
			ReDim Preserve arr(4,fs.Count+i-1)
			For Each k In fs
				arr(0,i) = k.Name
				arr(1,i) = formatSize(k.Size,s_sizeformat)
				arr(2,i) = k.DateLastModified
				arr(3,i) = Attr2Str(k.Attributes)
				arr(4,i) = k.Type
				i = i + 1
			Next
		End If
		Set fs = Nothing
		Set f = Nothing
		List = arr
		If Err.Number<>0 Then
			AB.Error.Msg = "(" & folderPath & ")"
			AB.Error.Raise 54
		End If
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.Lists(folderPath, fileType) (遍历)列出服务器上指定目录下所有的文件或文件夹
	'# @return: Array (数组)
	'# 			返回一个包含文件夹/文件属性的二维数组
	'# 			返回的属性包括：·名称 ·大小 ·最后更新时间 ·文件属性（文件夹无此值） ·类型
	'# @dowhat: (遍历)列出服务器上指定目录下所有的文件和文件夹
	'# 			用法和 AB.Fso.List(folderPath, fileType) 基本一样，唯一的区别是 “遍历”整个目录
	'--DESC------------------------------------------------------------------------------------
	'# @param folderPath: [String] (字符串)
	'# 文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'# @param fileType: [String] (字符串)
	'#  输出类型，可选值有：
	'#   0 或 ""，同时列出文件夹和文件；
	'#   1 或 "file"，只列出文件；
	'#   2 或 "folder"，只列出文件夹。
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim P,Flist,i : P = "/"
	'# Flist = AB.Fso.Lists(P,0) '同时列出文件夹和文件，也可写为：AB.Fso.Lists(P,"")
	'# 'Flist = AB.Fso.Lists(P,1) '只列出文件，也可写为：AB.Fso.Lists(P,"file")
	'# 'Flist = AB.Fso.Lists(P,2) '只列出文件夹，也可写为：AB.Fso.Lists(P,"folder")
	'# For i = 0 To UBound(Flist,2)
	'# 	AB.C.PrintCn "路径：" & Flist(0,i) &_
	'# 	" 大小：" & Flist(1,i) &_
	'# 	" 最后更新：" & Flist(2,i) &_
	'# 	" 属性：" & Flist(3,i) &_
	'# 	" 类型：" & Flist(4,i)
	'# Next
	'------------------------------------------------------------------------------------------

	Public Function Lists(ByVal folderPath, ByVal fileType)
		On Error Resume Next
		Call Lists_(folderPath, fileType, 0)
		Dim arr(), l, key, i, k, n, a : a = Array()
		If Not AB.C.isNul(o_flist) Then
			For Each key In o_flist
				If AB.C.CRight(key,":")="name" Then
					i = AB.C.CRight(AB.C.CLeft(key,":"),"f_")
					If Trim(i) <> "" Then
						k = CLng(i)
						ReDim Preserve a(k)
						a(k) = k
					End If
				End If
			Next
			ReDim Preserve arr(4,k)
			For Each n In a
				arr(0,n) = o_flist("f_"& n &":path")
				arr(1,n) = o_flist("f_"& n &":size")
				arr(2,n) = o_flist("f_"& n &":last")
				arr(3,n) = o_flist("f_"& n &":attr")
				arr(4,n) = o_flist("f_"& n &":type")
			Next
		End If
		Lists = arr
		Set o_flist = Nothing
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.glob(pattern, fileType) 匹配指定模式的文件名或目录 
	'# @return: Array (数组)
	'# 			返回一个包含文件夹/文件属性的二维数组
	'# 			返回的属性包括：·名称 ·大小 ·最后更新时间 ·文件属性（文件夹无此值） ·类型
	'# @dowhat: 匹配指定模式的文件名或目录
	'--DESC------------------------------------------------------------------------------------
	'# @param pattern: [String] (字符串)
	'# 规定检索模式。（匹配文件名里可以使用正则）
	'#  例如，/a/b/c/*.txt 匹配 /a/b/c/ 文件夹下的 所有.txt后缀的文件
	'#  例如，/a/b/c/*.* 匹配 /a/b/c/ 文件夹下的 所有带后缀的文件
	'#  例如，/a/b/c/[xyz]+* 匹配 /a/b/c/ 文件夹下的 所有带文字x或y或z开头的文件
	'# @param fileType: [String] (字符串)
	'#  输出类型，可选值有：
	'#   0 或 ""，同时列出文件夹和文件；
	'#   1 或 "file"，只列出文件；
	'#   2 或 "folder"，只列出文件夹。
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim P,Flist,i : P = "/a/b/c/*.asp"
	'# Flist = AB.Fso.glob(P,0) '同时列出文件夹和文件
	'# If Not AB.C.IsNul(Flist) Then
	'# 	For i = 0 To UBound(Flist,2)
	'# 		AB.C.PrintCn "路径：" & Flist(0,i) &_
	'# 		" 大小：" & Flist(1,i) &_
	'# 		" 最后更新：" & Flist(2,i) &_
	'# 		" 属性：" & Flist(3,i) &_
	'# 		" 类型：" & Flist(4,i)
	'# 	Next
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function glob(ByVal pattern, ByVal fileType)
		Dim arr(), n, rule, path, tmp, t1, t2, fst, fp, fname : path = pattern
		If isFolder(path) Then
			glob = Lists(path, fileType)
		Else
			path = AB.C.RP(path, "\", "/")
			If InStr(path,"/")>0 Then
				tmp = AB.C.RP(path, "\", "/")
				tmp = AB.C.IIF(Right(tmp,1)= "/", Left(tmp,Len(tmp)-1), tmp)
				t1 = Left(tmp, InStrRev(tmp,"/")-1)
				t2 = Right(tmp, Len(tmp)-InStrRev(tmp,"/"))
			Else
				t1 = "" : t2 = path
			End If
			t1 = AB.C.RP(t1, Array("\\","\/","//"), "/" )
			t2 = AB.C.RP(t2, Array(".","*","?"), Array("\.",".+",".?") )
			fst = Lists(t1, fileType)
			If Not AB.C.IsNul(fst) Then
				t1 = AB.C.IIF(inStr(t1,":")>0, t1, Server.MapPath(t1))
				t1 = AB.C.RP(t1,"/","\")
				t1 = AB.C.IIF(Right(t1,1)="\", t1, t1 & "\")
				n = 0
				For i = 0 To UBound(fst,2)
					fp = fst(0,i)
					'tmp = Split(AB.C.RP(fp,"/","\"), "/")
					'fname = tmp(Ubound(tmp))
					fname = AB.C.RP(fp,t1,"")
					rule = "^"& t2 & "$"
					If AB.C.RegTest(fname, rule) Then
						ReDim Preserve arr(4,n)
						arr(0,n) = fst(0,i)
						arr(1,n) = fst(1,i)
						arr(2,n) = fst(2,i)
						arr(3,n) = fst(3,i)
						arr(4,n) = fst(4,i)
						n = n+1
					End If
				Next
			End If
			glob = arr
		End If
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.fileName(file) 获取完整文件名
	'# @return: String (字符串) 返回完整文件名（带后缀）
	'# @dowhat: 调用此方法可得到文件的完整文件名，可以是服务器上不存在的文件
	'--DESC------------------------------------------------------------------------------------
	'# @param file: [String] (字符串)
	'# 文件名或文件路径，路径可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，可以是服务器上不存在的文件
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# AB.C.PrintCn AB.Fso.fileName("Test1.html") '返回结果：Test1.html
	'# AB.C.PrintCn AB.Fso.fileName("/abc/123/Test2.txt") '返回结果：Test2.txt
	'------------------------------------------------------------------------------------------

	Public Function fileName(ByVal f)
		fileName = GetNameOf(f, -1)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.NameOf(file) 获取文件名
	'# @return: String (字符串) 返回去掉后缀名的文件名
	'# @dowhat: 调用此方法可得到文件的文件名（去掉后缀名），可以是服务器上不存在的文件
	'--DESC------------------------------------------------------------------------------------
	'# @param file: [String] (字符串)
	'# 文件名或文件路径，路径可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，可以是服务器上不存在的文件
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# AB.C.PrintCn AB.Fso.NameOf("Test1.html") '返回结果：Test1
	'# AB.C.PrintCn AB.Fso.NameOf("/abc/123/Test2.txt") '返回结果：Test2
	'------------------------------------------------------------------------------------------

	Public Function NameOf(ByVal f)
		NameOf = GetNameOf(f, 0)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.ExtOf(file) 取文件扩展名（后缀名）
	'# @return: String (字符串) 返回文件扩展名（后缀名），如：.html，.asp，.txt
	'# @dowhat: 调用该方法可获取文件的扩展名（后缀名），可以是服务器上不存在的文件
	'--DESC------------------------------------------------------------------------------------
	'# @param file: [String] (字符串)
	'# 文件名或文件路径，路径可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，
	'# 可以是服务器上不存在的文件。
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# AB.C.PrintCn AB.Fso.ExtOf("Test1.html") '返回结果：.html
	'# AB.C.PrintCn AB.Fso.ExtOf("/abc/123/Test2.txt") '返回结果：.txt
	'------------------------------------------------------------------------------------------

	Public Function ExtOf(ByVal f)
		ExtOf = GetNameOf(f, 1)
	End Function

	Private Function GetNameOf(ByVal f, ByVal t)
		Dim re,na,ex
		If AB.C.isNul(f) Then GetNameOf = "" : Exit Function
		f = Replace(f,"\","/")
		If Right(f,1) = "/" Then
			If t=-1 Then : GetNameOf = "" : Exit Function : End If
			re = Split(f,"/")
			GetNameOf = AB.C.IIF(t=0,re(Ubound(re)-1),"")
			Exit Function
		ElseIf Instr(f,"/")>0 Then
			re = Split(f,"/")(Ubound(Split(f,"/")))
		Else
			re = f
		End If
		If t=-1 Then
			GetNameOf = re
			Exit Function
		End If
		If Instr(re,".")>0 Then
			na = Left(re,InstrRev(re,".")-1)
			ex = Mid(re,InstrRev(re,"."))
		Else
			na = re
			ex = ""
		End If
		If t = 0 Then
			GetNameOf = na
		ElseIf t = 1 Then
			GetNameOf = ex
		End If
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.Attr(path, attrType) 设置文件或文件夹属性
	'# @return: Boolean (布尔值) 设置成功返回真（True），失败返回假（False）
	'# @dowhat: 设置文件或文件夹属性
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串) 文件或文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'# @param attrType: [String] (字符串) 以英文逗号“,”或空格“ ”隔开的属性字符串，属性前的加号“+”表示增加此属性，减号“-”表示清除此属性。
	'# 可设置的属性如下：
	'#  R 只读文件属性
	'#  A 存档文件属性
	'#  S 系统文件属性
	'#  H 隐藏文件属性
	'--DEMO------------------------------------------------------------------------------------
	'# 可同时设置多个属性，多个属性用英文逗号“,”或空格“ ”隔开，属性前的加号“+”表示增加此属性，减号“-”表示清除此属性。如："+R,-H,-S,+A"
	'# 可设置的属性如下：
	'#  R 只读文件属性
	'#  A 存档文件属性
	'#  S 系统文件属性
	'#  H 隐藏文件属性
	'# ---------------
	'# AB.Use "Fso"
	'# Dim Result
	'#     Result = AB.Fso.Attr("/Test.txt","+R,-H")'设置Test.txt为只读并去除隐藏属性
	'# If Result Then
	'#     AB.C.Print "属性设置成功"
	'# Else
	'#     AB.C.Print "属性设置失败"
	'# End If
	'------------------------------------------------------------------------------------------

	Public Function Attr(ByVal path, ByVal attrType)
		On Error Resume Next
		Dim p,a,i,n,f,at : p = absPath(path) : n = 0 : Attr = True
		If not isExists(p) Then
			Attr = False
			AB.Error.Msg = "(" & path & ")"
			AB.Error.Raise 55
			Exit Function
		End If
		If isFile(p) Then
			Set f = Fso.GetFile(p)
		ElseIf isFolder(p) Then
			Set f = Fso.GetFolder(p)
		End If
		at = f.Attributes : a = UCase(attrType)
		If Instr(a,"+")>0 Or Instr(a,"-")>0 Then
			a = AB.C.IIF(Instr(a," ")>0,Split(a," "),Split(a,","))
			For i = 0 To Ubound(a)
				Select Case a(i)
					Case "+R" at = AB.C.IIF(at And 1,at,at+1)
					Case "-R" at = AB.C.IIF(at And 1,at-1,at)
					Case "+H" at = AB.C.IIF(at And 2,at,at+2)
					Case "-H" at = AB.C.IIF(at And 2,at-2,at)
					Case "+S" at = AB.C.IIF(at And 4,at,at+4)
					Case "-S" at = AB.C.IIF(at And 4,at-4,at)
					Case "+A" at = AB.C.IIF(at And 32,at,at+32)
					Case "-A" at = AB.C.IIF(at And 32,at-32,at)
				End Select
			Next
			f.Attributes = at
		Else
			For i = 1 To Len(a)
				Select Case Mid(a,i,1)
					Case "R" n = n + 1
					Case "H" n = n + 2
					Case "S" n = n + 4
				End Select
			Next
			f.Attributes = AB.C.IIF(at And 32,n+32,n)
		End If
		Set f = Nothing
		If Err.Number<>0 Then
			Attr = False
			AB.Error.Msg = "(" & path & ")"
			AB.Error.Raise 56
		End If
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.GetAttr(path, attrType) 获取文件或文件夹属性
	'# @return: String (字符串) 返回被查询文件或文件夹的属性值
	'# @dowhat: 调用此方法可以获取文件或者文件夹的属性，比如文件大小、类型、属性、创建日期等
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串)
	'#  文件或文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'# @param attrType: [String] (字符串)
	'#  要取得的属性类型，可选值有：
	'#   0 或 name ：名称
	'#   1 或 date 或 datemodified ：最后修改日期
	'#   2 或 datecreated ：创建日期
	'#   3 或 dateaccessed ：上次访问日期
	'#   4 或 size ：大小
	'#   5 或 attr ：文件属性（文件夹无此值）
	'#   6 或 type ：类型
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim P,i : P = "/Test/"
	'# AB.C.PrintCn AB.Fso.GetAttr(P,"date") '输出Test文件夹的最后修改日期
	'# P = "Test.html"
	'# For i = 0 To 6
	'# 	AB.C.PrintCn AB.Fso.GetAttr(P,i) '循环输出Test.html的各项属性
	'# Next
	'------------------------------------------------------------------------------------------

	Public Function getAttr(ByVal path, ByVal attrType)
		Dim f,s,p : p = absPath(path)
		If isFile(p) Then
			Set f = Fso.GetFile(p)
		ElseIf isFolder(p) Then
			Set f = Fso.GetFolder(p)
		Else
			getAttr = ""
			AB.Error.Msg = "(" & path & ")"
			AB.Error.Raise 57
			Exit Function
		End If
		Select Case LCase(attrType)
			Case "0","name" : s = f.Name
			Case "1","date", "datemodified" : s = f.DateLastModified
			Case "2","datecreated" : s = f.DateCreated
			Case "3","dateaccessed" : s = f.DateLastAccessed
			Case "4","size" : s = formatSize(f.Size,s_sizeformat)
			Case "5","attr" : s = Attr2Str(f.Attributes)
			Case "6","type" : s = f.Type
			Case Else s = ""
		End Select
		Set f = Nothing
		getAttr = s
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.Size(path) 获取文件或文件夹大小
	'# @return: Integer (整型) 返回文件或文件夹大小(字节)
	'# @dowhat: 获取文件或文件夹大小, 文件或文件夹不存在返回 -1
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串)
	'#  文件或文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# AB.C.PrintCn AB.Fso.Size("/Test/") '输出Test文件夹大小
	'# AB.C.PrintCn AB.Fso.Size("/Test/aaa.jpg") '输出路径“/Test/aaa.jpg”的文件大小
	'# '_可用AB.Fso.FormatSize函数对文件大小进行格式化显示, 可选值有：G,M,K,b,auto
	'# If AB.Fso.Size("/Test/aaa.jpg")>0 Then AB.C.PrintCn AB.Fso.FormatSize(AB.Fso.Size("/Test/aaa.jpg"), "K")
	'# '_或者使用以下方法：
	'# AB.Fso.SizeFormat = "M" '设置文件大小显示格式, 可选值有：G,M,K,b,auto，默认值为 K
	'# AB.C.PrintCn AB.Fso.getAttr("/Test/aaa.jpg","size")
	'------------------------------------------------------------------------------------------

	Public Function Size(ByVal path)
		Dim i_size,f,p : p = absPath(path) : i_size = 0
		If isFile(p) Then
			Set f = Fso.GetFile(p)
			i_size = f.Size
		ElseIf isFolder(p) Then
			Set f = Fso.GetFolder(p)
			i_size = f.Size
		Else
			i_size = -1
		End If
		Set f = Nothing
		Size = i_size
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.ImgWH(path) 获取图片宽度和高度信息
	'# @return: Array (数组) 该数组包含了图片宽度和高度元素
	'# @dowhat: 获取图片宽度和高度信息的数组
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串)
	'#  文件或文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim imgPath, wh, w, h
	'# imgPath = "/demo/test.jpg"
	'# wh = AB.Fso.ImgWH(imgPath)
	'# w = wh(0)
	'# h = wh(1)
	'# AB.C.Print ("<img src='"&imgPath&"' border=0><br>宽:"&w&";高:"&h)
	'------------------------------------------------------------------------------------------

	Public Function ImgWH(ByVal path)
		On Error Resume Next
		Dim p : p = absPath(path)
		ImgWH = o_imgwh.ImgWH(p)
		On Error Goto 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.CopyFile(fromPath, toPath) 复制文件（支持通配符 * 和 ? ）
	'# @return: Boolean (布尔值) 文件复制成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法可复制服务器上的单个或多个文件到指定的文件夹，支持通配符 * 和 ?
	'--DESC------------------------------------------------------------------------------------
	'# @param fromPath: [String] (字符串) 源路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，支持支持通配符 * 和 ? ，比如：
	'#  "/Test/1.asp"  复制Test文件夹下的1.asp
	'#  "/Test/*"  复制Test文件夹下的所有文件（不包括子文件夹）
	'#  "/Test/*.html"  复制Test文件夹下的所有后缀名是html的文件
	'#  "/Test/??st.ca?he"  复制Test文件夹下与规则匹配的文件（此规则可匹配Test.cache，list.cabhe等）
	'# @param toPath: [String] (字符串) 目标文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，不存在的文件夹将自动创建。
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '复制/Test/1/中的所有文件到/Test/2/
	'# Result = AB.Fso.CopyFile("/Test/1/*","/Test/2/")
	'# AB.C.Print "文件复制" & AB.C.IIF(Result,"成功","失败")
	'------------------------------------------------------------------------------------------

	Public Function CopyFile(ByVal fromPath, ByVal toPath)
		CopyFile = FOFO(fromPath,toPath,0,0)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.CopyFolder(fromPath, toPath) 复制文件夹（支持通配符 * 和 ? ）
	'# @return: Boolean (布尔值) 文件夹复制成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法可复制服务器上的单个或多个文件夹（包括子文件夹及子文件夹中的文件）到指定的文件夹，支持通配符 * 和 ?
	'--DESC------------------------------------------------------------------------------------
	'# @param fromPath: [String] (字符串) 文件夹源路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，支持支持通配符 * 和 ? ，比如：
	'#  "/Test/1/"  复制Test文件夹下的1文件夹
	'#  "/Test/*"  复制Test文件夹下的所有文件夹（包括子文件夹和子文件夹中的文件）
	'#  "/Test/T?/"  复制Test文件夹下与规则匹配的文件夹（此规则可匹配/Test/T1/，/Test/T2/等）
	'# @param toPath: [String] (字符串) 目标文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，不存在的文件夹将自动创建。
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '复制/Test/1/中的所有文件夹到/Test/2/
	'# Result = AB.Fso.CopyFolder("/Test/1/*","/Test/2/")
	'# AB.C.Print "文件复制" & AB.C.IIF(Result,"成功","失败")
	'------------------------------------------------------------------------------------------

	Public Function CopyFolder(ByVal fromPath, ByVal toPath)
		CopyFolder = FOFO(fromPath,toPath,1,0)
	End Function

	Public Function Copy(ByVal fromPath, ByVal toPath)
		Dim ff,tf : ff = absPath(fromPath) : tf = absPath(toPath)
		If isFile(ff) Then
			Copy = CopyFile(fromPath,toPath)
		ElseIf isFolder(ff) Then
			Copy = CopyFolder(fromPath,toPath)
		Else
			Copy = False
			AB.Error.Msg = "(" & fromPath & ")"
			AB.Error.Raise 58
		End If
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.MoveFile(fromPath, toPath) 移动文件（支持通配符 * 和 ? ）
	'# @return: Boolean (布尔值) 文件移动成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法可移动（剪切）服务器上的单个或多个文件到指定的文件夹，支持通配符 * 和 ?
	'--DESC------------------------------------------------------------------------------------
	'# @param fromPath: [String] (字符串)
	'#  源路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，支持通配符 * 和 ? ，比如：
	'#  "/Test/1.asp"  移动Test文件夹下的1.asp
	'#  "/Test/*"  移动Test文件夹下的所有文件（不包括子文件夹）
	'#  "/Test/*.html"  移动Test文件夹下的所有后缀名是html的文件
	'#  "/Test/??st.ca?he"  移动Test文件夹下与规则匹配的文件（此规则可匹配Test.cache，list.cabhe等）
	'# @param toPath: [String] (字符串)
	'# 目标文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，不存在的文件夹将自动创建 
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '移动/Test/1/中的所有文件到/Test/2/
	'# Result = AB.Fso.MoveFile("/Test/1/*","/Test/2/")
	'# AB.C.Print "文件移动" & AB.C.IIF(Result,"成功","失败")
	'------------------------------------------------------------------------------------------

	Public Function MoveFile(ByVal fromPath, ByVal toPath)
		MoveFile = FOFO(fromPath,toPath,0,1)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.MoveFolder(fromPath, toPath) 移动文件夹（支持通配符 * 和 ? ）
	'# @return: Boolean (布尔值) 文件移动成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法可移动(剪切)服务器上的单个或多个文件夹(包括子文件夹及子文件夹中的文件)到指定的文件夹,支持通配符 * 和 ?
	'--DESC------------------------------------------------------------------------------------
	'# @param fromPath: [String] (字符串)
	'#  文件夹源路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，支持通配符 * 和 ? ，比如：
	'#  "/Test/1/"  移动Test文件夹下的1文件夹
	'#  "/Test/*"  移动Test文件夹下的所有文件夹（包括子文件夹和子文件夹中的文件）
	'#  "/Test/T?/"  移动Test文件夹下与规则匹配的文件夹（此规则可匹配/Test/T1/，/Test/T2/等）
	'# @param toPath: [String] (字符串)
	'# 目标文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，不存在的文件夹将自动创建
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '移动/Test/1/中的所有文件夹到/Test/2/
	'# Result = AB.Fso.MoveFolder("/Test/1/*","/Test/2/")
	'# AB.C.Print "文件夹移动" & AB.C.IIF(Result,"成功","失败")
	'------------------------------------------------------------------------------------------

	Public Function MoveFolder(ByVal fromPath, ByVal toPath)
		MoveFolder = FOFO(fromPath,toPath,1,1)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.Move(fromPath, toPath) 移动文件或文件夹
	'# @return: Boolean (布尔值) 文件/文件夹移动成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法可移动服务器上的单个文件夹（包括文件夹中的子文件夹和文件）或文件到指定的文件夹，该方法不支持通配符 。
	'# 若需要使用通配符或移动多个文件夹/文件，请使用 AB.Fso.MoveFolder（文件夹）或 AB.Fso.MoveFile（文件）
	'--DESC------------------------------------------------------------------------------------
	'# @param fromPath: [String] (字符串)
	'# 文件/文件夹源路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头) 
	'# @param toPath: [String] (字符串)
	'# 目标文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头) 
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '移动/Test/1/11/到/Test/2/内
	'# Result = AB.Fso.Move("/Test/1/11/","/Test/2/")
	'# AB.C.Print "文件夹移动" & AB.C.IIF(Result,"成功","失败")
	'# '------
	'# '移动/Test/1/中的Test.html到/Test/2/内
	'# Result = AB.Fso.Move("/Test/1/Test.html","/Test/2/")
	'# AB.C.Print "文件移动" & AB.C.IIF(Result,"成功","失败")
	'------------------------------------------------------------------------------------------

	Public Function Move(ByVal fromPath, ByVal toPath)
		Dim ff,tf : ff = absPath(fromPath) : tf = absPath(toPath)
		If isFile(ff) Then
			Move = MoveFile(fromPath,toPath)
		ElseIf isFolder(ff) Then
			Move = MoveFolder(fromPath,toPath)
		Else
			Move = False
			AB.Error.Msg = "(" & fromPath & ")"
			AB.Error.Raise 59
		End If
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.DelFile(path) 删除文件（支持通配符 * 和 ? ）
	'# @return: Boolean (布尔值) 文件删除成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法可删除服务器上的单个或多个文件，支持通配符 * 和 ?
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串) 要删除的文件路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头))，支持通配符 * 和 ? ，比如：
	'# "/Test/1.asp"  删除Test文件夹下的1.asp
	'# "/Test/*"  删除Test文件夹下的所有文件（不包括子文件夹）
	'# "/Test/*.html"  删除Test文件夹下的所有后缀名是html的文件
	'# "/Test/??st.ca?he"  删除Test文件夹下与规则匹配的文件（此规则可匹配Test.cache，list.cabhe等）
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '删除Test文件夹中的所有文件
	'# Result = AB.Fso.DelFile("/Test/*")
	'# AB.C.Print "文件删除" & AB.C.IIF(Result,"成功","失败")
	'------------------------------------------------------------------------------------------

	Public Function DelFile(ByVal path)
		DelFile = FOFO(path,"",0,2)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.DelFolder(path) 删除文件夹（支持通配符 * 和 ? ）
	'# @alias: AB.Fso.RD(path)
	'# @return: Boolean (布尔值) 文件夹删除成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法可删除服务器上的单个或多个文件夹（包括子文件夹及子文件夹中的文件），支持通配符 * 和 ?
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串)
	'# 要删除的文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)，支持通配符 * 和 ? ，比如：
	'#  "/Test/Test1/"  删除Test文件夹下的Test1文件夹
	'#  "/Test/*"  删除Test文件夹下的所有文件夹（包括子文件夹和子文件夹中的文件）
	'#  "/Test/*est1/"  删除Test文件夹下所有与通配符匹配的文件夹（此规则可匹配Tabcest1，list1等）
	'#  "/Test/?sp?/"  删除Test文件夹下所有与通配符匹配的文件夹（此规则可匹配Asp等）
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '删除Test文件夹中的所有文件夹
	'# Result = AB.Fso.DelFolder("/Test/*")
	'# AB.C.Print "文件夹删除" & AB.C.IIF(Result,"成功","失败")
	'------------------------------------------------------------------------------------------

	Public Function DelFolder(ByVal path)
		DelFolder = FOFO(path,"",1,2)
	End Function

	Public Function RD(ByVal path)
		RD = DelFolder(path)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.Del(path) 删除文件或文件夹（不支持通配符）
	'# @return: Boolean (布尔值) 文件/文件夹删除成功返回真（True），失败返回假（False）
	'# @dowhat: 删除文件或文件夹, 删除成功返回True, 失败返回False
	'# 调用此方法可删除服务器上的单个文件夹（包括文件夹中的子文件夹和文件）或文件到指定的文件夹，该方法不支持通配符。
	'# 若需要使用通配符或删除多个文件夹/文件，请使用 AB.Fso.DelFolder（文件夹）或 AB.Fso.DelFile（文件）
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串)
	'# 要删除的文件/文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '删除Test文件夹中的Test1文件夹
	'# Result = AB.Fso.Del("/Test/Test1/")
	'# AB.C.Print "文件夹删除" & AB.C.IIF(Result,"成功","失败")
	'# '------------------------------
	'# '删除Test文件夹中的Test.html
	'# Result = AB.Fso.Del("/Test/Test.html")
	'# AB.C.Print "文件删除" & AB.C.IIF(Result,"成功","失败")
	'------------------------------------------------------------------------------------------

	Public Function Del(ByVal path)
		Dim p : p = absPath(path)
		If isFile(p) Then
			Del = DelFile(path)
		ElseIf isFolder(p) Then
			Del = DelFolder(path)
		Else
			Del = False
			AB.Error.Msg = "(" & path & ")"
			AB.Error.Raise 60
		End If
		Err.Clear()
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.Rename(path, newName) 重命名文件或文件夹
	'# @alias: AB.Fso.Ren(path, newName)
	'# @return: String (字符串) 文件/文件夹重命名成功返回真（True），失败返回假（False）
	'# @dowhat: 调用此方法可重命名服务器上的文件或文件夹
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串)
	'# 要重命名的文件/文件夹路径，可以是相对路径、站点绝对路径(以/开头)和硬盘绝对路径(以"盘符:\"开头)
	'# @param newName: [String] (字符串)
	'# 新文件夹名或新文件名（文件名包括后缀名）
	'--DEMO------------------------------------------------------------------------------------
	'# AB.Use "Fso"
	'# Dim Result
	'# '重命名Test文件夹为AspBox
	'# Result = AB.Fso.Rename("/Test/","AspBox")
	'# AB.C.PrintCn "文件夹重命名" & AB.C.IIF(Result,"成功","失败")
	'# '-----
	'# '重命名Test.html为AB.html
	'# Result = AB.Fso.Ren("/Test.html","AB.html")
	'# AB.C.PrintCn "文件重命名" & AB.C.IIF(Result,"成功","失败")
	'------------------------------------------------------------------------------------------

	Public Function Rename(ByVal path, ByVal newname)
		Dim p,n : p = absPath(path) : Rename = True
		n = Left(p,InstrRev(p,"\")) & newname
		If Not isExists(p) Then
			Rename = False
			AB.Error.Msg = "(" & path & ")"
			AB.Error.Raise 61
			Exit Function
		End If
		If isExists(n) Then
			Rename = False
			AB.Error.Msg = "(" & newname & ")"
			AB.Error.Raise 62
			Exit Function
		End If
		If isFolder(p) Then
			Fso.MoveFolder p,n
		ElseIf isFile(p) Then
			Copy p, n
			Del p
		End If
	End Function

	Public Function Ren(ByVal path, ByVal newname)
		Ren = Rename(path,newname)
	End Function

	Private Function absPath(ByVal p)
		Dim pt
		If AB.C.IsNul(p) Then absPath = "" : Exit Function
		If Mid(p,2,1)<>":" Then
			If isWildcards(p) Then
				p = Replace(p,"*","[.$.[a.b.s.t.a.r].#.]")
				p = Replace(p,"?","[.$.[a.b.q.u.e.s].#.]")
				p = Server.MapPath(p)
				p = Replace(p,"[.$.[a.b.q.u.e.s].#.]","?")
				p = Replace(p,"[.$.[a.b.s.t.a.r].#.]","*")
			Else
				p = Server.MapPath(p)
			End If
		Else
			If Left(LCase(Trim(p)),8) = "file:///" Then
				p = Trim(Replace(p,"file:///",""))
			End If
			p = Replace(p,"/","\")
		End If
		If Right(p,1) = "\" Then p = Left(p,Len(p)-1)
		absPath = p
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.Fso.MapPath(path) 获取文件或文件夹的物理绝对路径
	'# @return: String (字符串) 文件或文件夹的硬盘绝对路径(以"盘符:\"开头)
	'# @dowhat: 获取文件或文件夹的物理绝对路径, 此方法类似于Server.MapPath(path)。
	'--DESC------------------------------------------------------------------------------------
	'# @param path: [String] (字符串)
	'# 文件或文件夹的网络路径，可以是相对路径，也可以是站点绝对路径(以/开头)。
	'--DEMO------------------------------------------------------------------------------------
	'# none
	'------------------------------------------------------------------------------------------

	Public Function MapPath(p)
		MapPath = absPath(p)
	End Function

	Public Function formatSize(Byval fileSize, ByVal level)
		Dim s : s = Int(fileSize) : level = UCase(level)
		formatSize = AB.C.IIF(s/(1073741824)>0.01,FormatNumber(s/(1073741824),2,-1,0,-1),"0.01") & " GB"
		If s = 0 Then formatSize = "0 GB"
		If level = "G" Or (level="AUTO" And s>1073741824) Then Exit Function
		formatSize = AB.C.IIF(s/(1048576)>0.1,FormatNumber(s/(1048576),1,-1,0,-1),"0.1") & " MB"
		If s = 0 Then formatSize = "0 MB"
		If level = "M" Or (level="AUTO" And s>1048576) Then Exit Function
		formatSize = AB.C.IIF((s/1024)>1,Int(s/1024),1) & " KB"
		If s = 0 Then formatSize = "0 KB"
		If s = -1 Then formatSize = "-1 KB"
		If Level = "K" Or (level="AUTO" And s>1024) Then Exit Function
		If level = "B" or level = "AUTO" Then
			formatSize = s & " bytes"
		Else
			formatSize = s
		End If
	End Function

	Private Function isWildcards(ByVal path)
		isWildcards = False
		If Instr(path,"*")>0 Or Instr(path,"?")>0 Then isWildcards = True
	End Function

	Private Function FOFO(ByVal fromPath, ByVal toPath, ByVal FOF, ByVal MOC)
		On Error Resume Next
		FOFO = True
		Dim ff,tf,oc,of,oi,ot,os
		ff = absPath(fromPath) : tf = absPath(toPath)
		If FOF = 0 Then
			oc = isFile(ff) : of = "File" : oi = "文件"
		ElseIf FOF = 1 Then
			oc = isFolder(ff) : of = "Folder" : oi = "文件夹"
		End If
		If MOC = 0 Then
			ot = "Copy" : os = "复制"
		ElseIf MOC = 1 Then
			ot = "Move" : os = "移动"
		ElseIf MOC = 2 Then
			ot = "Delete" : os = "删除"
		End If
		If oc Then
			If MOC<>2 Then
				If FOF = 0 Then
					If Right(toPath,1)="/" or Right(toPath,1)="\" Then
						FOFO = MD(tf) : tf = tf & "\"
					Else
						FOFO = MD(Left(tf,InstrRev(tf,"\")-1))
					End If
				ElseIf FOF = 1 Then
					tf = tf & "\"
					FOFO = MD(tf)
				End If
				Execute("Fso."&ot&of&" ff,tf"&AB.C.IfThen(MOC=0,",b_overwrite"))
			Else
				Execute("Fso."&ot&of&" ff,b_force")
			End If
			If Err.Number<>0 Then
				FOFO = False
				AB.Error.Msg = "<br />" & os & oi & "失败！" & "( "&frompath&" "&AB.C.IIF(MOC=2,"",os&"到 "&toPath)&" )"
				AB.Error.Raise 63
			End If
		ElseIf isWildcards(ff) Then
			If MOC<>2 Then
				FOFO = MD(tf)
				Execute("Fso."&ot&of&" ff,tf"&AB.C.IIF(MOC=0,",b_overwrite",""))
			Else
				Execute("Fso."&ot&of&" ff,b_force")
			End If
			If Err.Number<>0 Then
				FOFO = False
				AB.Error.Msg = "<br />" & os & oi & "失败！" & "( "&frompath&" "&AB.C.IIF(MOC=2,"",os&"到 "&toPath)&" )"
				AB.Error.Raise 63
			End If
		Else
			FOFO = False
			AB.Error.Msg = "<br />" & os & oi & "失败！" & AB.C.IIF(MOC=2,"","源")&oi&"不存在( "&frompath&" )"
			AB.Error.Raise 63
		End If
		On Error Goto 0
	End Function

	Private Function Attr2Str(ByVal attrib)
		Dim a,s : a = Int(attrib)
		If a>=2048 Then a = a - 2048
		If a>=1024 Then a = a - 1024
		If a>=32 Then : s = "A" : a = a- 32 : End If
		If a>=16 Then a = a- 16
		If a>=8 Then a = a - 8
		If a>=4 Then : s = "S" & s : a = a- 4 : End If
		If a>=2 Then : s = "H" & s : a = a- 2 : End If
		If a>=1 Then : s = "R" & s : a = a- 1 : End If
		Attr2Str = s
	End Function

	Private Sub Lists_(ByVal folderPath, ByVal fileType, ByVal p)
		On Error Resume Next
		Dim f,fs,k,l
		folderPath = absPath(folderPath)
		Select Case LCase(fileType)
			Case "0","" l = 0
			Case "1","file" l = 1
			Case "2","folder" l = 2
			Case Else l = 0
		End Select
		If p = 0 Then
			i_list = 0 : Set o_flist = Server.CreateObject(AB.dictName)
		End If
		If isFolder(folderPath) Then
			Set f = Fso.GetFolder(folderPath)
			If l = 0 Or l = 2 Then
				Set fs = f.SubFolders
				For Each k In fs
					o_flist("f_"& i_list &":name") = k.Name & "/"
					o_flist("f_"& i_list &":path") = folderPath & "\" & k.Name
					o_flist("f_"& i_list &":size") = formatSize(k.Size,s_sizeformat)
					o_flist("f_"& i_list &":last") = k.DateLastModified
					o_flist("f_"& i_list &":attr") = Attr2Str(k.Attributes)
					o_flist("f_"& i_list &":type") = k.Type
					i_list = i_list + 1
					Call Lists_(folderPath & "\" & k.Name, l, 1)
				Next
			End If
			If l = 0 Or l = 1 Then
				Set fs = f.Files
				For Each k In fs
					o_flist("f_"& i_list &":name") = k.Name
					o_flist("f_"& i_list &":path") = folderPath & "\" & k.Name
					o_flist("f_"& i_list &":size") = formatSize(k.Size,s_sizeformat)
					o_flist("f_"& i_list &":last") = k.DateLastModified
					o_flist("f_"& i_list &":attr") = Attr2Str(k.Attributes)
					o_flist("f_"& i_list &":type") = k.Type
					i_list = i_list + 1
				Next
			End If
			Set fs = Nothing
			Set f = Nothing
		End If
		If Err.Number<>0 Then
			AB.Error.Msg = "(" & folderPath & ")"
			AB.Error.Raise 54
		End If
		On Error Goto 0
	End Sub

End Class

Class ImgWHInfo
	Private s_fsoName, s_steamName
	Private Sub Class_Initialize
		s_fsoName 	= AB.FsoName
		s_steamName	= AB.steamName
	End Sub
	Private Function Num2Str(ByVal Num,ByVal Base,ByVal Lens)
		Dim Ret
		Ret = ""
		While(Num>=Base)
			Ret = (Num Mod Base) & Ret
			Num = (Num - Num Mod Base)/Base
		Wend
		Num2Str = Right(String(Lens,"0") & Num & Ret,Lens)
	End Function
	Private Function Str2Num(ByVal Str,ByVal Base)
		Dim Ret,I
		Ret = 0
		For I=1 To Len(Str)
			Ret = Ret *base + Cint(Mid(Str,I,1))
		Next
		Str2Num=Ret
	End Function
	Private Function BinVal(ByVal Bin)
		Dim Ret,I
		Ret = 0
		For I = LenB(Bin) To 1 Step -1
			Ret = Ret *256 + AscB(MidB(Bin,I,1))
		Next
		BinVal=Ret
	End Function
	Private Function BinVal2(ByVal Bin)
		Dim Ret,I
		Ret = 0
		For I = 1 To LenB(Bin)
			Ret = Ret *256 + AscB(MidB(Bin,I,1))
		Next
		BinVal2=Ret
	End Function
	Private Function GetImageSize(ByVal filespec)
		Dim bFlag, Ret(3), o_steam
		Set o_steam = Server.CreateObject(s_steamName)
		With o_steam
			.Mode = 3
			.Type = 1 '以二进制模式(adTypeBinary)打开
			.Open
			.LoadFromFile filespec
		End With
		bFlag = o_steam.Read(3)
		Select Case Hex(binVal(bFlag))
			Case "4E5089":
				o_steam.Read(15)
				ret(0)="PNG"
				ret(1)=BinVal2(o_steam.Read(2))
				o_steam.Read(2)
				ret(2)=BinVal2(o_steam.Read(2))
			Case "464947":
				o_steam.read(3)
				ret(0)="GIF"
				ret(1)=BinVal(o_steam.Read(2))
				ret(2)=BinVal(o_steam.Read(2))
			Case "535746":
				o_steam.read(5)
				binData=o_steam.Read(1)
				sConv=Num2Str(ascb(binData),2,8)
				nBits=Str2Num(left(sConv,5),2)
				sConv=mid(sConv,6)
				While(len(sConv)<nBits*4)
					binData=o_steam.Read(1)
					sConv=sConv&Num2Str(AscB(binData),2,8)
				Wend
				ret(0)="SWF"
				ret(1)=Int(Abs(Str2Num(Mid(sConv,1*nBits+1,nBits),2)-Str2Num(Mid(sConv,0*nBits+1,nBits),2))/20)
				ret(2)=Int(Abs(Str2Num(Mid(sConv,3*nBits+1,nBits),2)-Str2Num(Mid(sConv,2*nBits+1,nBits),2))/20)
			Case "FFD8FF":
				Do
				Do: p1=binVal(o_steam.Read(1)): Loop While p1=255 And Not o_steam.EOS
				If p1>191 And p1<196 Then Exit Do Else o_steam.read(binval2(o_steam.Read(2))-2)
				Do:p1=binVal(o_steam.Read(1)):Loop While p1<255 And Not o_steam.EOS
				Loop While True
				o_steam.Read(3)
				ret(0)="JPG"
				ret(2)=binval2(o_steam.Read(2))
				ret(1)=binval2(o_steam.Read(2))
			Case Else:
				AB.Use "Char"
				If left(AB.Char.Bin2Str(bFlag),2)="BM" Then
					o_steam.Read(15)
					ret(0)="BMP"
					ret(1)=binval(o_steam.Read(4))
					ret(2)=binval(o_steam.Read(4))
				Else
					ret(0)=""
				End If
		End Select
		o_steam.Close
		Set o_steam = Nothing
		ret(3)="width=""" & ret(1) &""" height=""" & ret(2) &""""
		getimagesize=ret
	End Function
	''获取图片宽度和高度的类，支持JPG，GIF，PNG，BMP
	Public Function imgWH(ByVal IMGPath)
		Dim fso,IMGFile,FileExt,Arr
		Dim imgW,imgH: imgW = 0: imgH = 0
		Set fso = Server.CreateObject(s_fsoName)
		If (fso.FileExists(IMGPath)) Then
			Set IMGFile = fso.GetFile(IMGPath)
			FileExt=fso.GetExtensionName(IMGPath)
			Select Case FileExt
			Case "gif","bmp","jpg","png":
			Arr=GetImageSize(IMGFile.Path)
			imgW = Arr(1)
			imgH = Arr(2)
			End Select
			Set IMGFile=Nothing
		Else
			imgW = 0
			imgH = 0
		End If
		Set fso=Nothing
		imgWH = Array(imgW, imgH)
	End Function
End Class
%>