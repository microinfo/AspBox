<%
'######################################################################
'## ab.e.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Encrypt Block
'## Version     :   v1.0.1
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/03/21 3:05
'## Description :   AspBox Encrypt and Decrypt Block
'######################################################################

Class Cls_AB_E

	Public MD5,Aes,Base64,SDE,SoID,X,[XOR],RSA,SP,SHA1
	Private s_library, s_cores, s_nocores, s_ex
	Private o_lib
	Private b_core

	Private Sub Class_Initialize()
		s_ex 		= "ab.e"
		s_library 	= AB.CorePath
		b_core 		= True '是否预加载核心
		's_cores 	= "Md5,Aes,Base64,SDE,SoID,X,[XOR],RSA,SP,SHA1" '预定义核心
		s_nocores	= "" '指定不加载的预定义核心
		Init()
	End Sub

	Public Sub Init()
		On Error Resume Next
		Set o_lib = Server.CreateObject(AB.dictName)
		Core_Do "on", s_cores '加载预定义核心
		'[Load] "Md5,Aes,Base64" '载入Md5,AES,Base64加密核心
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Core_Do "off", s_cores
		[Drop] "Md5,Aes,Base64"
		ClearLib()
		Set o_lib = Nothing
	End Sub

	Public Property Let ifCore(ByVal s)
		b_core = s
	End Property
	Public Property Get ifCore()
		ifCore = b_core
	End Property

	Public Property Let baseCore(ByVal p)
		s_cores = p
	End Property
	Public Property Get baseCore()
		baseCore = s_cores
	End Property

	Public Property Let noCore(ByVal p)
		s_nocores = p
	End Property
	Public Property Get noCore()
		noCore = s_nocores
	End Property

	Public Function HasCore(ByVal s)
		On Error Resume Next
		Dim b : b = False
		Dim f : f = s
		f = Lcase(AB.C.RegReplace(f,"^\[(.*)\]$","$1"))
		If LCase(TypeName(Eval("["&f&"]"))) = "cls_ab_e_" & f Then
			b = True
		End IF
		HasCore = b
		On Error Goto 0
	End Function

	Private Sub Core_Do(ByVal t, ByVal s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim p, f, g, a
		Select Case t
			Case "on"
				IF b_core Then
					For i = 0 To Ubound(a_core)
						g = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
						f = Trim(Lcase(g))
						a = AB.Pub.CoresArray(s_nocores)
						If Not AB.Pub.InArray(f, a) Then
							p = s_library & "ab.e." & f & ".asp"
							'If Not AB.Pub.IsLoaded_Core(s_ex, f, o_lib) Then:Execute "Set [" & g & "] = New Cls_AB_E_Obj":End IF
							If Not AB.Pub.IsLoaded_Core(s_ex, f, o_lib) Then
								If AB.C.isFile(p) Then
									AB.Pub.CoreInclude s_ex, f, o_lib, p
									Execute("Set o_lib(""" & f & """) = New Cls_AB_E_" & g)
									If Lcase(TypeName(o_lib(""& f))) = "cls_ab_e_"& f Then Execute "Set [" & g & "] = o_lib(""" & f & """)"
								End If
							End If
						End If
					Next
				End IF
			Case "off"
				For i = Ubound(a_core) To 0 Step -1
					a_core(i) = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
					Execute "Set [" & a_core(i) & "] = Nothing"
					If o_lib.Exists(Lcase(a_core(i))) Then Execute "Set [" & o_lib(Lcase(a_core(i))) & "] = Nothing"
					o_lib.Remove(Lcase(a_core(i)))
				Next
		End Select
		On Error Goto 0
	End Sub

	Public Sub [Load](Byval s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim p, f, g
		For i = 0 To Ubound(a_core)
			If trim(a_core(i))<>"" Then
				g = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
				f = Lcase(g)
				p = s_library & "ab.e." & f & ".asp"
				If Not AB.Pub.IsLoaded_Core(s_ex, f, o_lib) Then
					If AB.C.isFile(p) Then
						AB.Pub.CoreInclude s_ex, f, o_lib, p
						Execute("Set o_lib(""" & f & """) = New Cls_AB_E_" & g)
						If Lcase(TypeName(o_lib(""& f))) = "cls_ab_e_"& f Then Execute "Set [" & g & "] = o_lib(""" & f & """)"
					End If
				End If
			End If
		Next
		On Error Goto 0
	End Sub

	Public Default Function [Lib](ByVal o)
		On Error Resume Next
		If isnull(o) or trim(o)="" Then : [Lib] = Empty : Exit Function : End If
		Dim loaded : loaded = False
		Dim p, f, g
		g = AB.C.RegReplace(Trim(o),"^\[(.*)\]$","$1")
		f = Lcase(g)
		p = s_library & "ab.e." & f & ".asp"
		If o_lib.Exists(""& f) and Lcase(TypeName(o_lib(""& f))) = "cls_ab_e_"& f Then
			loaded = True
			Set [Lib] = o_lib(""& f)
			Exit Function
		ElseIf Lcase(TypeName("AB.E.[" & g & "]")) = "cls_ab_e_"& f Then
			loaded = True
			'Execute("Set o_lib(""" & f & """) = New Cls_AB_E_" & f)
			Execute("Set o_lib(""" & f & """) = AB.E.[" & g & "]")
			Set [Lib] = o_lib(f)
			Exit Function
		Else
			loaded = False
			If AB.C.isFile(p) Then
				AB.Pub.CoreInclude s_ex, f, o_lib, p
				Execute("Set o_lib(""" & f & """) = New Cls_AB_E_" & f)
			Else
				AB.setError "(File Not Found: """ & p & """)", 4
			End If
		End If
		IF IsObject(o_lib(f)) Then Set [Lib] = o_lib(f)
		IF Err.Number<>0 Or Not IsObject(o_lib(f)) Then : [Lib] = Empty : Err.Clear : End IF
		On Error Goto 0
	End Function

	Public Sub Use(ByVal s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim p, f, g, t
		For i = 0 To Ubound(a_core)
			If trim(a_core(i))<>"" Then
				g = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
				f = Lcase(g)
				p = s_library & "ab.e." & f & ".asp"
				IF Not AB.Pub.IsLoaded_Core(s_ex, f, o_lib) Then
					t = Eval("LCase(TypeName([" & g & "]))")
					If t = "cls_ab_e_obj" Or t = "nothing" Or t = "empty" Or t = "null" Then
						If AB.C.isFile(p) Then
							AB.Pub.CoreInclude s_ex, f, o_lib, p
							Execute("Set o_lib(""" & f & """) = New Cls_AB_E_" & g)
							If Lcase(TypeName(o_lib(""& f))) = "cls_ab_e_"& f Then Execute "Set [" & g & "] = o_lib(""" & f & """)"
						Else
							AB.setError "(File Not Found: """ & p & """)", 4
						End If
					End If
				End IF
			End If
		Next
		On Error Goto 0
	End Sub

	Private Sub ClearLib()
		Dim i
		If AB.C.Has(o_lib) Then
			For Each i In o_lib
				Set o_lib(i) = Nothing
			Next
			o_lib.RemoveAll
		End If
	End Sub

	Public Sub [Drop](Byval s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim f, g
		For i = 0 To Ubound(a_core)
			If trim(a_core(i))<>"" Then
				g = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
				f = Lcase(g)
				If IsObject(o_lib) and Lcase(TypeName(o_lib))="dictionary" Then
					If o_lib.Exists(""&f) Then
						If IsObject(o_lib(""&f)) Then Execute("Set o_lib(""" & f & """) = Nothing")
						o_lib.Remove(""&f)
					End If
				End If
				Execute "Set [" & g & "] = Nothing"
			End If
		Next
		On Error Goto 0
	End Sub

	'@ ******************************************************************
	'@ 过程名:  AB.E.Escape String
	'@ 返  回:  把string中的特殊字符按Escape方式进行编码 # [String]
	'@ 作  用:  按Escape方式进行编码
	'==Param==============================================================
	'@ String  : 待编码的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.Escape("这是ASP") '返回值: %u8FD9%u662FASP
	'@ ******************************************************************

	Public Function Escape(ByVal ss)
		If AB.C.isNul(ss) Then Escape = "" : Exit Function
		Dim i,c,a,s : s = ""
		For i = 1 To Len(ss)
			c = Mid(ss,i,1)
			a = ASCW(c)
			If (a>=48 and a<=57) or (a>=65 and a<=90) or (a>=97 and a<=122) Then
				s = s & c
			ElseIf InStr("@*_+-./",c)>0 Then
				s = s & c
			ElseIf a>0 and a<16 Then
				s = s & "%0" & Hex(a)
			ElseIf a>=16 and a<256 Then
				s = s & "%" & Hex(a)
			Else
				s = s & "%u" & Hex(a)
			End If
		Next
		Escape = s
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.UnEscape String
	'@ 返  回:  把string中的%形式编码按UnEscape方式进行解码还原字符 # [String]
	'@ 作  用:  把含有%编码的字符串按UnEscape方式进行解码还原成特殊字符串
	'==Param==============================================================
	'@ String : 待解码的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.UnEscape("%u8FD9%u662FASP") '返回值: 这是ASP
	'@ ******************************************************************

	Public Function UnEscape(ByVal ss)
		If AB.C.isNul(ss) Then UnEscape = "" : Exit Function
		Dim x, s
		x = InStr(ss,"%")
		s = ""
		Do While x>0
			s = s & Mid(ss,1,x-1)
			If LCase(Mid(ss,x+1,1))="u" Then
				s = s & ChrW(CLng("&H"&Mid(ss,x+2,4)))
				ss = Mid(ss,x+6)
			Else
				s = s & Chr(CLng("&H"&Mid(ss,x+1,2)))
				ss = Mid(ss,x+3)
			End If
			x=InStr(ss,"%")
		Loop
		UnEscape = s & ss
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.encodeURIComponent String (实现js的encodeURIComponent函数功能)
	'@ 返  回:  对url进行编码后的字符串 # [String]
	'@ 作  用:  将中文、韩文等特殊字符转换成utf-8格式的url编码. 建议用此 encodeURIComponent方法 对url进行编码(比如用在xmlhttp.send)
	'@ 			# escape对0-255以外的unicode值进行编码时输出%u****格式，其它情况下escape，encodeURI，encodeURIComponent编码结果相同。
	'@ 			# escape不编码字符有69个：*, + , -, . , /, @, _, 0-9, a-z, A-Z
	'@ 			# encodeURI不编码字符有82个：!, #, $, &, ', (, ), *, +, ,, -, ., /, :, ;, =, ?, @, _, ~, 0-9, a-z, A-Z
	'@ 			# encodeURIComponent不编码字符有71个：!, ', (,), *, -, ., _, ~, 0-9, a-z, A-Z
	'==Param==============================================================
	'@ String  : 待编码的字符串 # [String]
	'==DEMO==============================================================
	'@ Dim str : str = "这是ASP,url:/admin/my.asp?user=admin&do=save"
	'@ AB.C.PrintCn str
	'@ '------- AB.E.Escape, AB.E.encodeURIComponent, AB.E.encodeURI 与 AB.E.URLEncode 比对:
	'@ AB.C.PrintCn AB.E.Escape(str) '输出: %u8FD9%u662FASP%2Curl%3A/admin/my.asp%3Fuser%3Dadmin%26do%3Dsave
	'@ AB.C.PrintCn AB.E.encodeURIComponent(str) '输出: %E8%BF%99%E6%98%AFASP%2Curl%3A%2Fadmin%2Fmy.asp%3Fuser%3Dadmin%26do%3Dsave
	'@ AB.C.PrintCn AB.E.encodeURI(str) '输出: %E8%BF%99%E6%98%AFASP,url:/admin/my.asp?user=admin&do=save
	'@ AB.C.PrintCn AB.E.URLEncode(str) '输出: %D5%E2%CA%C7ASP%2Curl%3A%2Fadmin%2Fmy%2Easp%3Fuser%3Dadmin%26do%3Dsave
	'@ AB.C.PrintCn AB.E.GBURLEncode(str) '输出: %D5%E2%CA%C7ASP,url:/admin/my.asp?user=admin&do=save
	'@ '------- 示例 -------
	'# AB.Use "Http"
	'# Dim h : Set h = AB.Http.New
	'# Dim url : url = "http://so.zhulang.com/search.php"
	'# h.Method = "POST"
	'# Dim key : key = "少年"
	'# AB.Use "E" : key = AB.E.encodeURIComponent(key)
	'# h.Data = "t=zh&k=" & key
	'# h.Charset = "UTF-8"
	'# Dim str : str = h.Post(url)
	'# Set h = Nothing
	'# AB.C.PrintCn str
	'@ ******************************************************************

	Function encodeURIComponent(ByVal s)
		AB.Use "Sc"
		Dim Sc : Set Sc = AB.Sc.New
		Sc.Lang = "js"
		Sc.Add "function jsEncodeURIComponent(s){return encodeURIComponent(s);}"
		'Dim obj : Set obj = Sc.Object
		temp = Sc.Object.jsEncodeURIComponent(s)
		'Set obj = Nothing
		Set Sc = Nothing
		encodeURIComponent = temp
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.decodeURIComponent String (实现js的decodeURIComponent函数功能)
	'@ 返  回:  对url字符串进行解码 # [String]
	'@ 作  用:  对url字符串进行解码还原
	'==Param==============================================================
	'@ String  : 待编码的字符串 # [String]
	'==DEMO==============================================================
	'@ Dim str : str = "这是ASP,url:/admin/my.asp?user=admin&do=save"
	'@ AB.C.Print AB.E.decodeURIComponent(AB.E.encodeURIComponent(str)) 
	'@ '输出: 这是ASP,url:/admin/my.asp?user=admin&do=save
	'@ ******************************************************************

	Function decodeURIComponent(ByVal s)
		AB.Use "Sc"
		Dim Sc : Set Sc = AB.Sc.New
		Sc.Lang = "js"
		Sc.Add "function jsDecodeURIComponent(s){return decodeURIComponent(s);}"
		'Dim obj : Set obj = Sc.Object
		temp = Sc.Object.jsDecodeURIComponent(s)
		'Set obj = Nothing
		Set Sc = Nothing
		decodeURIComponent = temp
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.encodeURI String (实现js的encodeURI函数功能)
	'@ 返  回:  返回一个编码的 URI # [String]
	'@ 作  用:  将文本字符串编码为一个有效的统一资源标识符
	'@ 			注意：encodeURI 方法不会对下列字符进行编码：":"、"/"、";" 和 "?"。
	'@ 			      请使用 encodeURIComponent 方法对这些字符进行编码。
	'==Param==============================================================
	'@ String  : 待编码的字符串 # [String]
	'==DEMO==============================================================
	'@ Dim str : str = "这是ASP,url:/admin/my.asp?user=admin&do=save"
	'@ AB.C.PrintCn str
	'@ AB.C.PrintCn AB.E.encodeURI(str) '输出: %E8%BF%99%E6%98%AFASP,url:/admin/my.asp?user=admin&do=save
	'@ ******************************************************************

	Function encodeURI(ByVal s)
		AB.Use "Sc"
		Dim Sc : Set Sc = AB.Sc.New
		Sc.Lang = "js"
		Sc.Add "function jsEncodeURI(s){return encodeURI(s);}"
		'Dim obj : Set obj = Sc.Object
		temp = Sc.Object.jsEncodeURI(s)
		'Set obj = Nothing
		Set Sc = Nothing
		encodeURI = temp
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.decodeURI String (实现js的decodeURI函数功能)
	'@ 返  回:  对url字符串进行解码 # [String]
	'@ 作  用:  将URI进行解码还原
	'==Param==============================================================
	'@ String  : 待编码的字符串 # [String]
	'==DEMO==============================================================
	'@ Dim str : str = "这是ASP,url:/admin/my.asp?user=admin&do=save"
	'@ AB.C.Print AB.E.decodeURI(AB.E.encodeURI(str)) 
	'@ '输出: 这是ASP,url:/admin/my.asp?user=admin&do=save
	'@ ******************************************************************

	Function decodeURI(ByVal s)
		AB.Use "Sc"
		Dim Sc : Set Sc = AB.Sc.New
		Sc.Lang = "js"
		Sc.Add "function jsDecodeURI(s){return decodeURI(s);}"
		'Dim obj : Set obj = Sc.Object
		temp = Sc.Object.jsDecodeURI(s)
		'Set obj = Nothing
		Set Sc = Nothing
		decodeURI = temp
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.URLEncode String (可取代Server.URLEncode函数)
	'@ 返  回:  把string中的特殊字符按Unicode编码 # [String]
	'@ 作  用:  按Unicode编码用%形式编码特殊字符串
	'==Param==============================================================
	'@ String  : 待编码的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.URLEncode("这是ASP") '返回值: %D5%E2%CA%C7ASP
	'@ ******************************************************************

	Public Function URLEncode(Byval strURL)
		On Error Resume Next
		Dim I,tempStr,nStr,sReturn
		For I = 1 To Len(strURL)
			nStr = Mid(strURL, I, 1)
			If Asc(nStr) < 0 Then
			   tempStr = "%" & Right(CStr(Hex(Asc(nStr))), 2)
			   tempStr = "%" & Left(CStr(Hex(Asc(nStr))), Len(CStr(Hex(Asc(nStr)))) - 2) & tempStr
			   sReturn = sReturn & tempStr
			ElseIf (Asc(nStr) >= 65 And Asc(nStr) <= 90) Or (Asc(nStr) >= 97 And Asc(nStr) <= 122) Then
			   sReturn = sReturn & nStr
			ElseIf (Asc(nStr) >= 48 And Asc(nStr) <= 57) Then
			   sReturn = sReturn & nStr
			Else
			   sReturn = sReturn & "%" & Hex(Asc(nStr))
			End If
		Next
		If Err<>0 Then
			sReturn = strURL
			Err.Clear
		End If
		URLEncode = sReturn
		On Error Goto 0
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.URLDecode String (即Server.URLEncode解码函数)
	'@ 返  回:  把string中含有%形式解码还原成特殊字符串 # [String]
	'@ 作  用:  把含有%编码的字符串按URLDecode方式进行解码还原成特殊字符串
	'==Param==============================================================
	'@ String  : 待解码的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.URLDecode("%D5%E2%CA%C7ASP") '返回值: 这是ASP
	'@ ******************************************************************

	Public Function URLDecode(enStr)
		On Error Resume Next
		Dim newStr : newStr = ""
		Dim haveChar,lastChar,i,char_c,next_1_c,next_1_num
		haveChar = False
		lastChar = ""
		For i = 1 To Len(enStr)
			char_c = Mid(enStr,i,1)
			If char_c="+" then
				newStr = newStr & " "
			ElseIf char_c="%" then
				If Err Then Err.Clear
				next_1_c = Mid(enStr,i+1,2)
				next_1_num = Cint("&H" & next_1_c)
				If Err.Number = 0 Then
					If haveChar Then
						haveChar = False
						newStr = newStr & Chr(Cint("&H" & lastChar & next_1_c))
					Else
						If abs(next_1_num)<=127 Then
							newStr = newStr & Chr(next_1_num)
						Else
							haveChar = True
							lastChar = next_1_c
						End If
					End If
					i=i+2
				Else
					newStr = newStr & "%"
				End If
			Else
				newStr = newStr & char_c
			End If
		Next
		URLDecode = newStr
		On Error Goto 0
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.GBURLEncode String (即Server.URLEncode解码函数)
	'@ 返  回:  把URL中含双字节字符转换为合法的URL传输字串 # [String]
	'@ 作  用:  转换双字节字符为合法的URL传输字串,单字节不做处理
	'==Param==============================================================
	'@ String  : 待处理的URL字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.GBURLEncode("这是ASP") '返回值: %D5%E2%CA%C7ASP
	'@ ******************************************************************

	Public Function GBURLEncode(byVal strURL) '
		Dim i,code
		GBURLEncode=""
		If trim(strURL)="" Then Exit Function
		For i=1 To len(strURL)
			code=Asc(Mid(strURL,i,1))
			If code<0 Then code=code+65536
			If code>255 Then
				GBURLEncode=GBURLEncode&"%"&Left(Hex(Code),2)&"%"&Right(Hex(Code),2)
			Else
				GBURLEncode=GBURLEncode&Mid(strURL,i,1)
			End if
		Next
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.FromUnicode(str)
	'@ 返  回:  字符串 # [String]
	'@ 作  用:  FromUnicode（实现VB的StrConv(str, vbFromUnicode)方法）
	'==Param==============================================================
	'@ str  : 字符串 # [String]
	'==DEMO==============================================================
	'@ AB.C.Print AB.E.ConvUnicode(AB.E.FromUnicode("强力Power鎯头Hammer"))
	'@ ******************************************************************

	Public Function FromUnicode(ByVal str)
		On Error Resume Next
		Dim objStm
		Set objStm = Server.CreateObject(AB.steamName)
		With objStm
			.Charset = "GBK"
			.Type = 2
			.Open
			.WriteText str
			.Position = 0
			.Charset = "Unicode"
			.Type = 1
			FromUnicode = MidB(.Read, 1)
		End With
		On Error Goto 0
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.ConvUnicode(str)
	'@ 返  回:  字符串 # [String]
	'@ 作  用:  Unicode（实现VB的StrConv(str, vbUnicode)方法）
	'==Param==============================================================
	'@ str  : 字符串 # [String]
	'==DEMO==============================================================
	'@ AB.C.Print AB.E.ConvUnicode(AB.E.FromUnicode("强力Power鎯头Hammer"))
	'@ AB.C.Print AB.E.FromUnicode(AB.E.ConvUnicode("强力Power鎯头Hammer"))
	'@ ******************************************************************

	Public Function ConvUnicode(ByVal str)
		On Error Resume Next
		Dim rs, stm, bytAry, intLen
		If Len(str & "") > 0 Then
			str = MidB(str, 1)
			intLen = LenB(str)
			Set rs = Server.CreateObject("ADODB.Recordset")
			Set stm = Server.CreateObject(AB.steamName)
			With rs
				.Fields.Append "X", 205, intLen
				.Open
				.AddNew
				rs(0).AppendChunk str & ChrB(0)
				.Update
				bytAry = rs(0).GetChunk(intLen)
			End With
			With stm
				.Type = 1
				.Open
				.Write bytAry
				.Position = 0
				.Type = 2
				.Charset = "GBK"
				ConvUnicode = .ReadText
			End With
		End If
		stm.Close
		Set stm = Nothing
		rs.Close
		Set rs = Nothing
		On Error Goto 0
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.HTMLEncode String (可取代Server.HTMLEncode函数)
	'@ 返  回:  对字符串进行HTMLEncode编码 # [String]
	'@ 作  用:  一般将字符串中的(")编码为(&quot;)、(&)编码为(&amp;)、(<)编码为(&lt;)、(>)编码为(&gt;)
	'==Param==============================================================
	'@ String  : 待编码的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.HTMLEncode("<这是ASP>") '返回值: &lt;这是ASP&gt;
	'@ ******************************************************************

	Public Function HTMLEncode(Byval str)
		Dim sEncode
		sEncode = str
		sEncode = AB.C.RP(sEncode, "&", "&amp;")
		sEncode = AB.C.RP(sEncode, """", "&quot;")
		sEncode = AB.C.RP(sEncode, "'", "&#39;")
		sEncode = AB.C.RP(sEncode, ">", "&gt;")
		sEncode = AB.C.RP(sEncode, "<", "&lt;")
		sEncode = AB.C.RP(sEncode, " ", "&nbsp;")
		HTMLEncode = sEncode
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.HTMLDecode String (即Server.URLEncode解码函数)
	'@ 返  回:  把string中含有%形式解码还原成特殊字符串 # [String]
	'@ 作  用:  把含有%编码的字符串按HTMLDecode方式进行解码还原成特殊字符串
	'==Param==============================================================
	'@ String  : 待解码的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.HTMLDecode("&lt;这是ASP&gt;") '返回值: <这是ASP>
	'@ ******************************************************************

	Public Function HTMLDecode(Byval str)
		Dim sEncode
		sEncode = str
		sEncode = AB.C.RP(sEncode, "&amp;", "&")
		sEncode = AB.C.RP(sEncode, "&quot;", """")
		sEncode = AB.C.RP(sEncode, "&#39;", "'")
		sEncode = AB.C.RP(sEncode, "&gt;", ">")
		sEncode = AB.C.RP(sEncode, "&lt;", "<")
		sEncode = AB.C.RP(sEncode, "&nbsp;", " ")
		HTMLDecode = sEncode
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.xEncode String
	'@ 返  回:  加密后的字符串 # [String]
	'@ 作  用:  对字符串进行编码加密
	'==Param=============================================================
	'@ String  : 待编码的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.xEncode("!#$%&"" '()*+,.-_/:;<=>?@[\]^`{|}~中文")
	'@ ******************************************************************

	Public Function xEncode(Byval s)
		Dim p:p = s
		'p = AB.E.URLEncode(p)
		'p = AB.E.Base64.E(p)
		p = AB.E.X.Smp_Encode(p)
		xEncode = p
	End Function

	'@ ******************************************************************
	'@ 过程名:  AB.E.xDecode String
	'@ 返  回:  解密后的字符串(原字符串) # [String]
	'@ 作  用:  对加密字符串进行解密
	'==Param=============================================================
	'@ String  : 待解码的字符串 # [String]
	'==DEMO==============================================================
	'@ AB.E.xDecode(AB.E.xEncode("!#$%&"" '()*+,.-_/:;<=>?@[\]^`{|}~中文"))
	'@ ******************************************************************

	Public Function xDecode(Byval s)
		Dim p:p = s
		'p = AB.E.URLDecode(p)
		'p = AB.E.Base64.D(p)
		p = AB.E.X.Smp_Decode(p)
		xDecode = p
	End Function

End Class

Class Cls_AB_E_Obj : End Class
%>