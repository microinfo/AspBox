<%
'######################################################################
'## ab.h.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox H Class
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2013/02/18 2:14
'## Description :   AspBox 系统函数集成模块
'## 				(为解决自定义函数与系统函数名称相同产生冲突的问题)
'######################################################################

Class Cls_AB_H

	Private o_tmp

	Private Sub Class_Initialize()
		On Error Resume Next
		Set o_tmp = New Cls_AB_H_Tmp
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		On Error Resume Next
		Set o_tmp = Nothing
		On Error Goto 0
	End Sub

	'---------------------------------------------------------------------
	'# AB.H.Split(sText,sSplit)
	'# @return: Integer
	'# @dowhat: 字符串分割为数组,此方法是原Split函数的补充
	'# 			当分割字符sSplit值为空时，返回以每个字符组成的数组
	'--DESC---------------------------------------------------------------
	'# @param sText [string] : 原字符串
	'# @param sSplit [string / Integer] : 分割字符
	'--DEMO---------------------------------------------------------------
	'# AB.Trace AB.H.Split("a,b,c",",") '返回：Array("a","b","c")
	'# AB.Trace AB.H.Split("abcd","") '返回：Array("a","b","c","d")
	'# AB.Trace AB.H.Split("abcdefg",3) '返回：Array("abc","def","g")
	'# AB.Trace AB.H.Split("abcd",0) '返回：Array("a","b","c","d")
	'#--------------------------------------------------------------------

	Function Split(Byval s, Byval c)
		Split = o_tmp.strSplit(s, c)
	End Function

	'---------------------------------------------------------------------
	'# AB.H.Len(str) 即为Len系统函数的同名函数
	'#--------------------------------------------------------------------

	Function [Len](Byval s)
		[Len] = o_tmp.strLen(s)
	End Function

	'---------------------------------------------------------------------
	'# AB.H.Cstr(str) 即为Cstr系统函数的同名函数
	'#--------------------------------------------------------------------

	Function [Cstr](Byval s)
		[Cstr] = o_tmp.strCstr(s)
	End Function

	'---------------------------------------------------------------------
	'# AB.H.Trim(str) 即为Trim系统函数的同名函数
	'#--------------------------------------------------------------------

	Function [Trim](Byval s)
		[Trim] = o_tmp.strTrim(s)
	End Function

	'---------------------------------------------------------------------
	'# AB.H.LTrim(str) 即为Trim系统函数的同名函数
	'#--------------------------------------------------------------------

	Function [LTrim](Byval s)
		[LTrim] = o_tmp.strLTrim(s)
	End Function

	'---------------------------------------------------------------------
	'# AB.H.RTrim(str) 即为RTrim系统函数的同名函数
	'#--------------------------------------------------------------------

	Function [RTrim](Byval s)
		[RTrim] = o_tmp.strRTrim(s)
	End Function

	'---------------------------------------------------------------------
	'# AB.H.Join(str) 即为Join系统函数的同名函数
	'#--------------------------------------------------------------------

	Function [Join](Byval p, Byval s)
		[Join] = o_tmp.strJoin(p, s)
	End Function

End Class

Class Cls_AB_H_Tmp

	Function strLen(Byval s)
		strLen = Len(s)
	End Function

	Function strCstr(Byval s)
		strCstr = Cstr(s)
	End Function

	Function strTrim(Byval s)
		strTrim = Trim(s)
	End Function

	Function strLTrim(Byval s)
		strLTrim = LTrim(s)
	End Function

	Function strRTrim(Byval s)
		strRTrim = RTrim(s)
	End Function

	Function strSplit(Byval s, Byval c)
		On Error Resume Next
		Dim str, p : str = s : p = c
		Dim temp,i,a()
		If AB.C.IsNul(str) Then:Err.Clear:strSplit=Array():Exit Function:End If
		str = Cstr(str) : If Err Then:Err.Clear:strSplit=Array():Exit Function:End If
		If AB.C.isInt(p) Then
			Dim n,k:k=0:n=CLng(c)
			If n>0 Then
				For i=0 To Len(str)-1 Step n
					ReDim Preserve a(k)
					a(k) = Mid(str, i+1, n)
					k=k+1
				Next
				temp = a
			Else
				For i=0 To Len(str)-1
					ReDim Preserve a(i)
					a(i) = Mid(str, i+1, 1)
				Next
				temp = a
			End If
		Else
			If Len(p)>0 Then
				temp = Split(str, p)
			Else
				For i=0 To Len(str)-1
					ReDim Preserve a(i)
					a(i) = Mid(str, i+1, 1)
				Next
				temp = a
			End If
		End If
		strSplit = temp
		On Error Goto 0
	End Function

	Function strJoin(Byval p, Byval s)
		strJoin = Join(p, s)
	End Function

End Class
%>